package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"encoding/json"
	"net/http"
	"os"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/registry"
)

// catalogue godoc
// @Router /catalogue [get]
// @Tags  catalogue
// @Summary Internal catalogue
// @Description Complete set of artifacts and types
// @Produce  json
// @Success		200
func Catalogue(w http.ResponseWriter, r *http.Request) {
	common.Logger.Info("Requested the catalogue")
	w.WriteHeader(http.StatusAccepted)
	// TODO: how to implement user based...?
	w.Header().Set("Content-Type", "application/json")
	regURL := os.Getenv("REGISTRY_URL")
	regPass := os.Getenv("REGISTRY_ADMIN_PASSWORD")
	regUser := os.Getenv("REGISTRY_ADMIN_USER")
	if regURL == "" || regPass == "" || regUser == "" {
		w.WriteHeader(http.StatusInternalServerError)
		msg := `{"error": "Registry not configured"}`
		payload, _ := json.Marshal(msg)
		_, _ = w.Write(payload)
		return
	}
	reg, err := registry.NewRegWithStaticCredentials(regURL, regUser, regPass)
	if err != nil {
		common.Logger.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		msg := `{"error": "Registry not reachable"}`
		payload, _ := json.Marshal(msg)
		_, _ = w.Write(payload)
		return
	}
	artifactList, err := registry.List(reg)
	if err != nil {
		common.Logger.Error(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		msg := `{"error": "List of artifacts not found"}`
		payload, _ := json.Marshal(msg)
		_, _ = w.Write(payload)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	payload, _ := json.Marshal(artifactList)
	_, _ = w.Write(payload)

	/*
		TODO: Add filter of artifact type
		// Obtain the name from the path
		artifact := r.URL.Query().Get("artifact")
		// Check if 'artifact' parameter is not empty
		if artifact == "" {
			http.Error(w, "Artifact parameter is required", http.StatusBadRequest)
			return
		}
		returnCode, data := endpoints.GetReportData(artifact)
	*/
}
