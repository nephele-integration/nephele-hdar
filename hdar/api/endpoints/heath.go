package endpoints

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"fmt"
	"net/http"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

// Health godoc
// @Router /health [get]
// @Tags  internal
// @Summary Health check of the service
// @Description Provides start checks for K8s
// @Produce  plain
// @Success		200
func Health(w http.ResponseWriter, r *http.Request) {
	common.Logger.Info("Requested a heath check")
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprintf(w, "Status OK")
}
