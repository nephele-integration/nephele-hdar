package artifact

import (
	"embed"
	"errors"
	"path/filepath"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/descriptor"

	nephele_descriptor "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/hdar/pkg/descriptor"
)

// To embed files in the executable (schema and templates)
//
//go:embed docs/*
var nepheleFiles embed.FS

var MediaTypeHDAGConfig = "application/vnd.nephele.hdag.config.v1+json"

/*
This is fully customizable and should allow us to implement adaptations for OSM, TOSCA and others.
Each case should obtain the info from the corresponding descriptor.
*/
type HDAGConfig struct {
	// SchemaVersion to xNFRV version: 1 = [v0.3.0, ]. Mandatory in all implementations
	SchemaVersion string `json:"schemaVersion"`
	// Config MediaType. Mandatory in all implementations
	MediaType string `json:"mediaType"`
	// Reference to the specification of the CSAR
	PackageSpec string `json:"packageSpec"`
	// Reference to the specification of the content's structure
	DataModelSpec string `json:"dataModelSpec"`
	// Path to the core descriptor file
	DescriptorFile string `json:"descriptorFile"`
	// Mandatory in all implementations and needs to match GetTemplateMap keys
	DescriptorType string `json:"descriptorType"`
	// Unique identifier of the descriptor for the same descriptorType
	DescriptorId string `json:"descriptorId"`
	// Version of the descriptor
	DescriptorVersion string `json:"descriptorVersion"`
	// Dependencies of the descriptor
	Dependencies []descriptor.Dependency `json:"dependencies"`
}

// Used for Artifacts of Nephele HDA Components
// implements ArtifactInspector from xNFRV
type HdaGraphArtifactInspector struct {
}

func NewHdaGraphArtifactInspector() HdaGraphArtifactInspector {
	return HdaGraphArtifactInspector{}
}

func (ai HdaGraphArtifactInspector) GetDescriptorInspector(a *artifact.Artifact) descriptor.DescriptorInspector {
	i := &nephele_descriptor.HdaGraphDescriptorInspector{
		Raw: a.GetDescriptorFileRaw().Data,
	}
	err := i.LoadContent()
	if err != nil {
		return nil
	}
	return i
}

func (ai HdaGraphArtifactInspector) IsDescriptorFileSuffix(fName string) bool {
	return strings.HasSuffix(fName, ".yaml") || strings.HasSuffix(fName, ".yml")
}

func (ai HdaGraphArtifactInspector) GetConfig(a *artifact.Artifact) interface{} {
	config := &HDAGConfig{
		SchemaVersion: "1",
		PackageSpec:   "",
		DataModelSpec: "",
		MediaType:     ai.GetMediaType(),
		Dependencies:  a.Dependencies,
	}
	dInspector := ai.GetDescriptorInspector(a)
	dRaw := a.GetDescriptorFileRaw()
	config.DescriptorFile = filepath.Join(dRaw.Dir, dRaw.Name)
	config.DescriptorType = dInspector.GetDescriptorType()
	config.DescriptorId = dInspector.GetUniqueName().Value
	config.DescriptorVersion = dInspector.GetVersion().Value
	return config
}

func (ai HdaGraphArtifactInspector) GetMediaType() string {
	return MediaTypeHDAGConfig
}

func (ai HdaGraphArtifactInspector) VerifyStructure(arc *archive.Archive) error {
	// Only one Yaml or yml file allowed in the base folder and one in the intent folder
	okBase := false
	okIntent := false
	common.Logger.Debugf("Len '%d'\n", len(arc.ContentRaw))
	for _, fRaw := range arc.ContentRaw {
		rootDir, subDir := filepath.Split(filepath.Dir(fRaw.Dir))
		common.Logger.Debugf("Checking '%s' '%s' '%s'\n", rootDir, subDir, fRaw.Name)
		if rootDir == "" && subDir != "" && ai.IsDescriptorFileSuffix(fRaw.Name) {
			if okBase {
				common.Logger.Warning("More than one Yaml file was found in intent folder")
				okBase = false
				break
			}
			okBase = true
		} else if rootDir != "" && subDir != "" && ai.IsDescriptorFileSuffix(fRaw.Name) {
			if okIntent {
				common.Logger.Warning("More than one Yaml file was found in base folder")
				okIntent = false
				break
			}
			okIntent = true
		} else {
			// This is not the core descriptor since it is in a subfolder
			// Or does not have the right suffix
			common.Logger.Debugf("Verify Structure found unknown file %s\n", fRaw.Name)
		}

	}
	if !okBase || !okIntent {
		return errors.New("Incorrect structure of files")
	}
	return nil
}

func (ai HdaGraphArtifactInspector) GetSchemaMap() map[string]string {
	hdagData, _ := nepheleFiles.ReadFile("docs/hdag_schema.json")
	return map[string]string{
		"HDAG": string(hdagData),
	}
}

func (ai HdaGraphArtifactInspector) GetTemplateMap() map[string]string {
	hdagData, _ := nepheleFiles.ReadFile("docs/hdag.templ")
	return map[string]string{
		"HDAG": string(hdagData),
	}
}
