// Keep as a different package to enforce testing as a consumer of the lib
package artifact_test

import (
	"testing"

	// Installed as a consumer
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/hdar/pkg/artifact"
	xnfr_artifact "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
)

func TestInferType(t *testing.T) {
	filepath1 := "../../resources/hda_graph_wot_1-1.0.0.tar.gz"
	a1, err := xnfr_artifact.NewLocalArtifact(filepath1)
	if err != nil {
		t.Error(err.Error())
	}
	found := artifact.AddCorrectInspector(a1, false)
	if !found {
		t.Error("Unable to load descriptor with inspector provided.")
	} else if a1.DescriptorType != "HDAG" {
		t.Error("Unable to load descriptor with inspector provided.")
	}
}
