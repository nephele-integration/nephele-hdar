package artifact

import (
	"embed"
	"errors"
	"fmt"
	"path/filepath"
	"strings"

	"helm.sh/helm/v3/pkg/lint"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	artifact "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
	xnfr_descriptor "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/descriptor"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/hdar/pkg/descriptor"
)

// To embed files in the executable (schema and templates)
//
//go:embed docs/*
var wotFiles embed.FS

var MediaTypeWoTConfig = "application/vnd.nephele.vo.wot.config.v1+json"

/*
This is fully customizable and should allow us to implement adaptations for any VO implementation.
Each case should obtain the info from the corresponding descriptor.
*/
type WoTConfig struct {
	// SchemaVersion to xNFRV version: 1 = [v0.3.0, ]. Mandatory in all implementations
	SchemaVersion string `json:"schemaVersion"`
	// Config MediaType. Mandatory in all implementations
	MediaType string `json:"mediaType"`
	// Reference to the specification of the CSAR
	PackageSpec string `json:"packageSpec"`
	// Reference to the specification of the content's structure
	DataModelSpec string `json:"dataModelSpec"`
	// Path to the core descriptor file
	DescriptorFile string `json:"descriptorFile"`
	// Mandatory in all implementations and needs to match GetTemplateMap keys
	DescriptorType string `json:"descriptorType"`
	// Unique identifier of the descriptor for the same descriptorType
	DescriptorId string `json:"descriptorId"`
	// Version of the descriptor
	DescriptorVersion string `json:"descriptorVersion"`
	// Dependencies of the descriptor
	Dependencies []xnfr_descriptor.Dependency `json:"dependencies"`
}

// Used for Artifacts of Nephele Virtual Object in WoT implementation
// implements ArtifactInspector from xNFRV
type WoTArtifactInspector struct {
}

func NewWoTArtifactInspector() WoTArtifactInspector {
	return WoTArtifactInspector{}
}

func (ai WoTArtifactInspector) GetDescriptorInspector(a *artifact.Artifact) xnfr_descriptor.DescriptorInspector {
	dFileRaw := a.GetDescriptorFileRaw("Chart.yaml")
	if dFileRaw.Name == "" {
		dFileRaw = a.GetDescriptorFileRaw("Chart.yml")
	}
	if dFileRaw.Name == "" {
		common.Logger.Errorf("Unable to load Descriptor Content\n")
		return nil
	}
	i := &descriptor.WoTDescriptorInspector{
		Raw: dFileRaw.Data,
	}
	mapPointer, err := archive.LoadYamlContent(i.Raw)
	if err != nil {
		common.Logger.Errorf("Unable to load Descriptor Content: %s\n", err.Error())
		return nil
	}
	i.Content = mapPointer
	return i
}

func (ai WoTArtifactInspector) IsDescriptorFileSuffix(fName string) bool {
	return strings.HasSuffix(fName, ".yaml") || strings.HasSuffix(fName, ".yml")
}

func (ai WoTArtifactInspector) GetArtifactType() string {
	return "HELM"
}

func (ai WoTArtifactInspector) GetConfig(a *artifact.Artifact) interface{} {
	config := &WoTConfig{
		SchemaVersion: "1",
		PackageSpec:   "",
		DataModelSpec: "",
		MediaType:     ai.GetMediaType(),
		Dependencies:  a.Dependencies,
	}
	dInspector := ai.GetDescriptorInspector(a)
	dRaw := a.GetDescriptorFileRaw()
	config.DescriptorFile = filepath.Join(dRaw.Dir, dRaw.Name)
	config.DescriptorType = dInspector.GetDescriptorType()
	config.DescriptorId = dInspector.GetUniqueName().Value
	config.DescriptorVersion = dInspector.GetVersion().Value
	return config
}

func (ai WoTArtifactInspector) GetMediaType() string {
	return MediaTypeWoTConfig
}

func (ai WoTArtifactInspector) VerifyStructure(arc *archive.Archive) error {
	// It should be a helm chart with expected folders and manifests
	// ToDo Pending. Might be good to run https://github.com/helm/helm/blob/main/pkg/lint
	l := lint.All(arc.FileRaw.Dir, map[string]interface{}{}, "", false)
	if len(l.Messages) > 0 {
		fullMsg := ""
		for _, msg := range l.Messages {
			if msg.Severity == 4 {
				fullMsg = fmt.Sprintf("\t-%s\n", msg.Err.Error())
			}
		}
		if fullMsg != "" {
			return errors.New(fullMsg)
		}
	}
	expectedFiles := map[string]bool{
		"values.yaml":                   false,
		"Chart.yaml":                    false,
		"scripts/app.py":                false,
		"scripts/config.yaml":           false,
		"scripts/td.json":               false,
		"templates/configmap.yaml":      false,
		"templates/deployment.yaml":     false,
		"templates/service.yaml":        false,
		"templates/hpa.yaml":            false,
		"templates/ingress.yaml":        false,
		"templates/serviceaccount.yaml": false,
		"templates/_helpers.tpl":        false,
	}
	for _, fRaw := range arc.ContentRaw {
		_, after, _ := strings.Cut(fRaw.Dir, "/")
		name := fmt.Sprintf("%s%s", after, fRaw.Name)
		common.Logger.Debugf("testing %s: ", name)
		if val, ok := expectedFiles[name]; ok {
			if val {
				common.Logger.Debugf("File duplicated\n")
				return errors.New(fmt.Sprintf("%s Duplicated", name))
			}
			expectedFiles[name] = true
			common.Logger.Debugf("Setting to true\n")
			continue
		}
		common.Logger.Debugf("not found in expected files")
	}
	for key := range expectedFiles {
		if !expectedFiles[key] {
			common.Logger.Debugf("Should have found filed %s\n", key)
			return errors.New(fmt.Sprintf("Should have found filed %s\n", key))
		}
	}
	return nil
}

func (ai WoTArtifactInspector) GetSchemaMap() map[string]string {
	wotData, _ := nepheleFiles.ReadFile("docs/wot_schema.json")
	return map[string]string{
		"WOT": string(wotData),
	}
}

func (ai WoTArtifactInspector) GetTemplateMap() map[string]string {
	wotData, _ := nepheleFiles.ReadFile("docs/wot.templ")
	return map[string]string{
		"WOT": string(wotData),
	}
}
