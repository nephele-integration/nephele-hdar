package descriptor

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	xnfr_descriptor "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/descriptor"
)

// Implements the DescriptorInspector interface
type HdaGraphDescriptorInspector struct {
	Raw     []byte
	Content archive.MapStr
}

func (di *HdaGraphDescriptorInspector) GetDescriptorContent() archive.MapStr {
	return di.Content
}

// Helper method for artifact inspector to know if it is the right one
// Returning "" means that this is not the expected Inspector interface to use
func (di HdaGraphDescriptorInspector) GetDescriptorType() string {
	content := string(di.Raw)
	if strings.Contains(strings.ToLower(content), "hdagraph:") {
		return "HDAG"
	}
	common.Logger.Infof("Not recognize as HDA Graph Descriptor\n")
	return ""
}

func (di *HdaGraphDescriptorInspector) LoadContent() error {
	// Directly called in HdaGraphArtifactInspector
	mapPointer, err := archive.LoadYamlContent(di.Raw)
	if err != nil {
		return err
	}
	di.Content = mapPointer
	return nil
}

func (di *HdaGraphDescriptorInspector) GetAnnotations() xnfr_descriptor.ReturnMapStringsToString {
	annotations := make(map[string]string)
	content := di.Content["hdaGraph"].(archive.MapStr)
	if val, ok := content["description"]; ok {
		annotations["org.opencontainers.image.description"] = val.(string)
	}
	if val, ok := content["designer"]; ok {
		annotations["org.opencontainers.image.authors"] = val.(string)
	}
	// Cannot be added to Manifest annotations as it breaks the fileStore
	if val, ok := content["id"]; ok {
		annotations["org.opencontainers.image.title"] = val.(string)
	} else {
		common.Logger.Info("Unable to detect id\n")
		return xnfr_descriptor.ReturnMapStringsToString{
			Value: annotations,
			Error: errors.New("Unable to detect id"),
		}
	}
	if val, ok := content["version"]; ok {
		// Recover .0 if it was lost during unmarshal (1.0 -> 1), (1.1 -> 1.1)
		if strings.Contains(val.(string), ".") {
			annotations["org.opencontainers.image.version"] = val.(string)
		} else if version, err := strconv.ParseFloat(val.(string), 1); err == nil {
			if fmt.Sprintf("%.1f", version) == "0.0" {
				annotations["org.opencontainers.image.version"] = "0"
			} else {
				annotations["org.opencontainers.image.version"] = fmt.Sprintf("%.1f", version)
			}
		} else {
			common.Logger.Info("Unable to detect version\n")
		}
	} else {
		common.Logger.Info("Unable to detect version\n")
		return xnfr_descriptor.ReturnMapStringsToString{
			Value: annotations,
			Error: errors.New("Unable to detect version"),
		}
	}

	// TODO: Pending to figure out how to get + org.opencontainers.image.licenses
	return xnfr_descriptor.ReturnMapStringsToString{
		Value: annotations,
		Error: nil,
	}
}

func (di *HdaGraphDescriptorInspector) GetUniqueName() xnfr_descriptor.ReturnString {
	if val, ok := di.Content["hdaGraph"].(archive.MapStr)["id"]; ok && val.(string) != "" {
		return xnfr_descriptor.ReturnString{
			Value: val.(string),
			Error: nil,
		}
	} else {
		return xnfr_descriptor.ReturnString{
			Value: "",
			Error: errors.New("Unable to obtain unique name from HDA Graph descriptor\n"),
		}
	}
}

func (di *HdaGraphDescriptorInspector) GetVersion() xnfr_descriptor.ReturnString {

	if val, ok := di.Content["hdaGraph"].(archive.MapStr)["version"]; ok && val.(string) != "" {
		// Recover .0 if it was lost during unmarshal (1.0 -> 1), (1.1 -> 1.1)
		var v string
		if strings.Contains(val.(string), ".") {
			v = val.(string)
		} else if version, err := strconv.ParseFloat(val.(string), 1); err == nil {
			if fmt.Sprintf("%.1f", version) == "0.0" {
				v = "0"
			} else {
				v = fmt.Sprintf("%.1f", version)
			}
		} else {
			v = ""
			common.Logger.Info("Unable to detect version\n")
		}

		return xnfr_descriptor.ReturnString{
			Value: v,
			Error: nil,
		}
	} else {
		return xnfr_descriptor.ReturnString{
			Value: "",
			Error: errors.New("Unable to obtain version from HDA Component"),
		}
	}
}

func (di *HdaGraphDescriptorInspector) GetDependencies() []xnfr_descriptor.Dependency {
	var dependencies []xnfr_descriptor.Dependency
	// TODO implement
	return dependencies
}
