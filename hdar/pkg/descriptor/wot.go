package descriptor

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	xnfr_descriptor "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/descriptor"
)

// Implements the DescriptorInspector interface
type WoTDescriptorInspector struct {
	Raw     []byte
	Content archive.MapStr
}

func (di *WoTDescriptorInspector) GetDescriptorContent() archive.MapStr {
	return di.Content
}

// Helper method for artifact inspector to know if it is the right one
// Returning "" means that this is not the expected Inspector interface to use
func (di WoTDescriptorInspector) GetDescriptorType() string {
	if strings.Contains(strings.ToLower(string(di.Raw)), "implementer: wot") {
		return "WOT"
	}
	common.Logger.Infof("Not recognize as VO WoT Descriptor\n")
	return ""
}

func (di *WoTDescriptorInspector) GetAnnotations() xnfr_descriptor.ReturnMapStringsToString {
	annotations := make(map[string]string)
	if val, ok := di.Content["description"]; ok {
		annotations["org.opencontainers.image.description"] = val.(string)
	}
	if val, ok := di.Content["maintainers"]; ok {
		annotations["org.opencontainers.image.authors"] = val.([]interface{})[0].(archive.MapStr)["name"].(string)
	}
	// Cannot be added to Manifest annotations as it breaks the fileStore
	if val, ok := di.Content["name"]; ok {
		annotations["org.opencontainers.image.title"] = val.(string)
	} else {
		common.Logger.Info("Unable to detect id\n")
		return xnfr_descriptor.ReturnMapStringsToString{
			Value: annotations,
			Error: errors.New("Unable to detect id"),
		}
	}
	if val, ok := di.Content["version"]; ok {
		// Recover .0 if it was lost during unmarshal (1.0 -> 1), (1.1 -> 1.1)
		if strings.Contains(val.(string), ".") {
			annotations["org.opencontainers.image.version"] = val.(string)
		} else if version, err := strconv.ParseFloat(val.(string), 1); err == nil {
			if fmt.Sprintf("%.1f", version) == "0.0" {
				annotations["org.opencontainers.image.version"] = "0"
			} else {
				annotations["org.opencontainers.image.version"] = fmt.Sprintf("%.1f", version)
			}
		} else {
			common.Logger.Info("Unable to detect version\n")
		}
	} else {
		common.Logger.Info("Unable to detect version\n")
		return xnfr_descriptor.ReturnMapStringsToString{
			Value: annotations,
			Error: errors.New("Unable to detect version"),
		}
	}

	// TODO: Pending to figure out how to get + org.opencontainers.image.licenses
	return xnfr_descriptor.ReturnMapStringsToString{
		Value: annotations,
		Error: nil,
	}
}

func (di *WoTDescriptorInspector) GetUniqueName() xnfr_descriptor.ReturnString {
	if val, ok := di.Content["name"]; ok && val.(string) != "" {
		return xnfr_descriptor.ReturnString{
			Value: val.(string),
			Error: nil,
		}
	} else {
		return xnfr_descriptor.ReturnString{
			Value: "",
			Error: errors.New("Unable to obtain unique name from WoT Chart descriptor\n"),
		}
	}
}

func (di *WoTDescriptorInspector) GetVersion() xnfr_descriptor.ReturnString {

	if val, ok := di.Content["version"]; ok && val.(string) != "" {
		// Recover .0 if it was lost during unmarshal (1.0 -> 1), (1.1 -> 1.1)
		var v string
		if strings.Contains(val.(string), ".") {
			v = val.(string)
		} else if version, err := strconv.ParseFloat(val.(string), 1); err == nil {
			if fmt.Sprintf("%.1f", version) == "0.0" {
				v = "0"
			} else {
				v = fmt.Sprintf("%.1f", version)
			}
		} else {
			v = ""
			common.Logger.Info("Unable to detect version\n")
		}

		return xnfr_descriptor.ReturnString{
			Value: v,
			Error: nil,
		}
	} else {
		return xnfr_descriptor.ReturnString{
			Value: "",
			Error: errors.New("Unable to obtain version from HDA Component"),
		}
	}
}

func (di *WoTDescriptorInspector) GetDependencies() []xnfr_descriptor.Dependency {
	var dependencies []xnfr_descriptor.Dependency
	// TODO implement
	return dependencies
}
