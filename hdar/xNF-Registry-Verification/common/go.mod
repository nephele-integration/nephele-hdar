module github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common

go 1.19

replace github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common/ => ./

require github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
