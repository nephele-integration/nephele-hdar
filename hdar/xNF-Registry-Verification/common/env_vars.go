package common

import (
	"os"
	"github.com/joho/godotenv"
)

// Load environment variables if the file exists
func LoadEnvFile(envFile string) {
	// Load .env file if exists
	file, err := os.OpenFile(envFile, os.O_RDONLY, 0644)
	if err == nil {
		godotenv.Load(envFile)
		defer file.Close()
	} else {
		Logger.Warningf("Unable to read env file %s, %s\n", envFile, err.Error())
	}
}
