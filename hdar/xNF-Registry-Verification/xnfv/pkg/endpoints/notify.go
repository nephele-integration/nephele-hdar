package endpoints

import (
	"net/http"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfv/pkg/models"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfv/pkg/redis"
)

// Based on the list of events received filter out those that are not push. Let the listeners ignore
// since they are many more.
func ProcessEvents(n *models.Notification) (httpCode int) {
	// Load publisher
	publisher := redis.NewPublisher()
	for _, event := range n.Events {
		if event.Action != "push" {
			// Our only interest right now is on push events
			common.Logger.Debugf("Ignoring %s for manifest %s\n", event.Action, event.Target.Repository)
			continue
		}
		common.Logger.Infof("Processing %s for manifest %s\n", event.Action, event.Target.Repository)
		common.Logger.Debugf("%+v\n", event)
		// publish event
		publisher.PublishPushEvent(event)
		common.Logger.Info("Event sent")

	}
	return http.StatusAccepted
}
