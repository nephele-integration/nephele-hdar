package endpoints

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

func GetReportData(artifact string) (int, []byte) {
	// Make sure it is correctly formatted
	// Should check that there is no info about registry?
	aComp := strings.Split(artifact, ":")
	if len(aComp) != 2 {
		return http.StatusBadRequest, []byte("Artifact needs to contain the version/tag and no references to registry base URL")
	}
	// When the report is added as layer we will use the tag, right now it is hardcoded to report-artifactId:latest
	project, repo := "", strings.Split(artifact, ":")[0]
	i := strings.LastIndex(artifact, "/")
	if i != -1 {
		// there is a project prefix-> harbor
		project = artifact[:i+1]
		repo = artifact[i+1:]
	}
	// not adding the tag because it defaults to latest
	reportId := fmt.Sprintf("report-%s", repo)
	defer os.RemoveAll(reportId)
	fullPath := fmt.Sprintf("%s/%s%s", os.Getenv("REGISTRY_URL"), project, reportId)
	common.Logger.Debugf("Checking %s, %s = %s\n", project, repo, fullPath)
	common.Logger.Infof("Pulling report from %s\n", fullPath)
	cmd := exec.Command("xnfrv", "pull", fullPath, "--untar")
	output, err := cmd.CombinedOutput()
	if err != nil {
		msg := fmt.Sprintf("Cannot pull artifact: %s -> %s\n", err.Error(), string(output))
		common.Logger.Error(msg)
		return http.StatusInternalServerError, []byte(msg)
	}
	common.Logger.Debugf("Pull output: %s\n", string(output))

	data, err := os.ReadFile(filepath.Join(reportId, "report.yaml"))
	if err != nil {
		msg := fmt.Sprintf("Cannot find report: %s -> %s\n", filepath.Join(reportId, "report.yaml"), err.Error())
		common.Logger.Errorf(msg)
		return http.StatusInternalServerError, []byte(msg)
	}
	return http.StatusOK, data

}
