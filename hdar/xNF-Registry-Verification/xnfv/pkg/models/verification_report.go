package models

var LintMessage = "Static analysis of artifact: syntax, semantics, structure, format."

type TestReportCommon struct {
	ArtifactID       string   `yaml:"artifactId"`
	ArtifactVersion  string   `yaml:"artifactVersion"`
	ArtifactFullRepo string   `yaml:"artifactFullRepo"`
	Checksum         string   `yaml:"checksum"`
	Description      string   `yaml:"description"`
	Tool             string   `yaml:"tool"`
	Success          bool     `yaml:"success"`
	Logs             []string `yaml:"logs"`
}

type VerificationReport struct {
	Id        string           `yaml:"id"`
	Timestamp int64            `yaml:"timestamp"`
	Lint      TestReportCommon `yaml:"lint"`
}

func NewVerificationReport() *VerificationReport {
	return &VerificationReport{
		Lint: TestReportCommon{
			Description: LintMessage,
		},
	}
}
