// Keep as a different package to enforce testing as a consumer of the lib
package models_test

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v2"

	// Installed as a consumer
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfv/pkg/models"
)

func TestGenerateAndVerifyVReport(t *testing.T) {
	expectedYAML := `id: report-someId
timestamp: 1702478636
lint:
	artifactId: someId
	artifactVersion: 1.0.1
	artifactFullRepo: reg/repo/someid:1.0.1
	checksum: somechecksum
	description: some description
	tool: xnfrv
	success: false
	logs:
	- some message
`

	vReport := models.NewVerificationReport()
	vReport.Id = "report-someId"
	vReport.Timestamp = 1702478636 // time.Now().Unix()
	vReport.Lint.ArtifactID = "someId"
	vReport.Lint.ArtifactVersion = "1.0.1"
	vReport.Lint.ArtifactFullRepo = "reg/repo/someid:1.0.1"
	vReport.Lint.Checksum = "somechecksum"
	vReport.Lint.Description = "some description"
	vReport.Lint.Tool = "xnfrv"
	vReport.Lint.Success = false
	vReport.Lint.Logs = []string{"some message"}
	// Test write to file
	data, err := yaml.Marshal(vReport)
	assert.NoError(t, err)
	err = os.WriteFile("output.yaml", data, 0644)
	defer os.Remove("output.yaml")
	assert.NoError(t, err)
	assert.Equal(t, expectedYAML, strings.ReplaceAll(string(data), "  ", "\t"))
	// Test read from file
	data2, err := os.ReadFile("output.yaml")
	assert.NoError(t, err)
	var vReport2 models.VerificationReport
	err = yaml.Unmarshal(data2, &vReport2)
	assert.NoError(t, err)
	assert.EqualValues(t, vReport, &vReport2)
}
