package redis

import (
	"fmt"
	"os"

	"github.com/hibiken/asynq"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

const (
	// Only One type of message required right now
	PushEvent = "xnfv:manifestPushEvent"
)

type RedisConnection struct {
	Listener  *asynq.Server
	Publisher *asynq.Client
	Mux       *asynq.ServeMux
	opts      asynq.RedisClientOpt
}

// Server
func NewRedisConnection() (*RedisConnection, error) {
	common.LoadEnvFile(".env")
	ip := os.Getenv("REDIS_IP")
	port := os.Getenv("REDIS_PORT")
	// Create and configuring Redis connection.
	common.Logger.Info(fmt.Sprintf("%s:%s", ip, port))
	opts := asynq.RedisClientOpt{
		Addr: fmt.Sprintf("%s:%s", ip, port), // Redis server address
	}

	// Create and configuring Asynq pub and sub but keep disconnected.
	listener := asynq.NewServer(opts, asynq.Config{
		// Specify how many concurrent workers to use.
		Concurrency: 10,
		// Specify multiple queues with different priority.
		// Will be using critical for verification pipelines.
		Queues: map[string]int{
			"critical": 6, // processed 60% of the time
			"default":  3, // processed 30% of the time
			"low":      1, // processed 10% of the time
		},
	})
	return &RedisConnection{
		Listener: listener,
		Mux:      asynq.NewServeMux(),
		// Publisher is only created on demand
		opts: opts,
	}, nil
}

func (r *RedisConnection) InitPublisher() {
	r.Publisher = asynq.NewClient(r.opts)
}
