package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"github.com/hibiken/asynq"
	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	"gopkg.in/yaml.v2"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfv/pkg/models"
)

type RedisListener struct {
	r *RedisConnection
}

func NewListener() (*RedisListener, error) {
	r, e := NewRedisConnection()
	if e != nil {
		common.Logger.Error(e.Error())
		return nil, e
	}
	l := &RedisListener{
		r: r,
	}
	l.configureListener()
	return l, nil
}

// Worker listener
func (l *RedisListener) configureListener() {
	// Define a task handler per topic
	l.r.Mux.HandleFunc(
		PushEvent,         // task type
		l.HandlePushEvent, // handler function
	)
}

func (l *RedisListener) HandlePushEvent(ctx context.Context, t *asynq.Task) error {
	common.Logger.Info("Received an event")
	var event models.Event
	report := models.NewVerificationReport()
	err := json.Unmarshal(t.Payload(), &event)
	if err != nil {
		common.Logger.Error("Event unmarsharl error %s\n", err.Error())
		return err
	}
	// Find out what type of artifact it is
	// All events are for targe.MediaType="application/vnd.oci.image.manifest.v1+json"

	// Pull the artifact's manifests
	repoURL := fmt.Sprintf("%s/%s:%s", os.Getenv("REGISTRY_URL"), event.Target.Repository, event.Target.Tag)
	common.Logger.Debugf("repoURL: %s\n", repoURL)
	cmd := exec.Command("xnfrv", "manifest", repoURL)
	output, err := cmd.CombinedOutput()
	if err != nil {
		common.Logger.Errorf("Cannot pull manifests: %s -> %s\n", err.Error(), string(output))
		return err
	}
	common.Logger.Debug(string(output))
	// Read the image manifest content (name convention from xNFRV CLI repo)
	imageId := strings.Split(event.Target.Repository, "/")[len(strings.Split(event.Target.Repository, "/"))-1]
	manifestId := strings.ReplaceAll(event.Target.Repository, "/", "-")
	imageManifest := fmt.Sprintf("./manifest-image-%s.json", manifestId)
	configManifest := fmt.Sprintf("./manifest-config-%s.json", manifestId)
	content, err := os.ReadFile(imageManifest)
	defer os.Remove(imageManifest)
	defer os.Remove(configManifest)
	report.Lint.ArtifactID = imageId
	report.Lint.ArtifactVersion = event.Target.Tag
	report.Lint.ArtifactFullRepo = repoURL
	report.Id = fmt.Sprintf("report-%s", imageId)
	report.Timestamp = time.Now().Unix()
	if err != nil {
		common.Logger.Errorf("Error when opening file: %s\n", err.Error())
		return err
	}
	common.Logger.Debug(string(content))
	common.Logger.Debugf("report: %+v\n", report)
	// Now let's unmarshall the data into `payload`
	var payload v1.Manifest
	err = json.Unmarshal(content, &payload)
	if err != nil {
		common.Logger.Errorf("Error when opening file: %s\n", err.Error())
		return err
	}
	configMediaType := payload.Config.MediaType
	if configMediaType == "application/vnd.etsi.nfv.config.v1+json" {
		common.Logger.Debug("Detected NFV image event")
		cmd := exec.Command("xnfrv", "pull", repoURL)
		output, err := cmd.CombinedOutput()
		if err != nil {
			common.Logger.Errorf("Cannot pull artifact: %s -> %s\n", err.Error(), string(output))
			return err
		}
		common.Logger.Debug(string(output))
		// Name convention from xNFRV CLI repo (could be taken from image Manifest's tar.gz layer)
		artifactName := fmt.Sprintf("%s-%s.tar.gz", imageId, event.Target.Tag)
		common.Logger.Debugf("artifactName for lint: %s\n", artifactName)
		cmd = exec.Command("xnfrv", "lint", artifactName)
		output, err = cmd.CombinedOutput()
		report.Lint.Tool = "xnfrv"
		os.Remove(artifactName)
		if err != nil {
			report.Lint.Success = false
			report.Lint.Logs = append(report.Lint.Logs, err.Error())
			report.Lint.Logs = append(report.Lint.Logs, string(output))
			common.Logger.Errorf("Cannot lint artifact with xnfrv: %s -> %s\n", err.Error(), string(output))
		} else {
			report.Lint.Success = true
			common.Logger.Debug(string(output))
		}
		common.Logger.Debugf("Lint report: %+v\n", report)
	} else if configMediaType == "application/vnd.cncf.helm.config.v1+json" {
		common.Logger.Debug("Detected Helm image event")
		report.Lint.Tool = "helm"
		// Fix helm push with --plain-http
		helmRepoURL := strings.TrimPrefix(repoURL, "http://")
		helmRepoURL = strings.TrimPrefix(repoURL, "https://")
		cmd := exec.Command("helm", "pull", fmt.Sprintf("OCI://%s", helmRepoURL))
		output, err := cmd.CombinedOutput()
		if err != nil {
			common.Logger.Errorf("Cannot pull artifact: %s -> %s\n", err.Error(), string(output))
			return err
		}
		common.Logger.Debug(string(output))
		// Name convention from Helm
		artifactName := fmt.Sprintf("%s-%s.tgz", imageId, event.Target.Tag)
		cmd = exec.Command("helm", "lint", artifactName)
		output, err = cmd.CombinedOutput()
		os.Remove(artifactName)
		if err != nil {
			report.Lint.Success = false
			report.Lint.Logs = append(report.Lint.Logs, err.Error())
			report.Lint.Logs = append(report.Lint.Logs, string(output))
			common.Logger.Errorf("Cannot lint artifact with helm: %s -> %s\n", err.Error(), string(output))
			return err
		} else {
			report.Lint.Success = true
			report.Lint.Logs = append(report.Lint.Logs, string(output))
			common.Logger.Debug(string(output))
		}
	} else {
		common.Logger.Infof("Detected %s image event\n", configMediaType)
		return nil
	}
	// Create report artifact
	common.Logger.Debug("Creating report artifact")
	data, err := yaml.Marshal(report)
	err = os.MkdirAll(report.Id, 0777)
	if err != nil {
		common.Logger.Infof("Unable to create the desired destination path: %s\n", err.Error())
		return err
	}
	err = os.WriteFile(filepath.Join(report.Id, "report.yaml"), data, 0644)
	cmd = exec.Command("xnfrv", "package", "tar", report.Id)
	output, err = cmd.CombinedOutput()
	// Push does not work if artifactId folder is in Workdir due to ORAS issue
	os.RemoveAll(report.Id)
	if err != nil {
		common.Logger.Errorf("Package package error: %s -> %s\n", err.Error(), string(output))
		return err
	}
	// Push to the same base repository
	reportFullRepoName := os.Getenv("REGISTRY_URL")
	lastIndex := strings.LastIndex(event.Target.Repository, "/")
	if lastIndex != -1 {
		reportFullRepoName = reportFullRepoName[:lastIndex]
	}
	reportArtifactName := fmt.Sprintf("%s-%s.tar.gz", report.Id, "latest")
	defer os.Remove(reportArtifactName)
	common.Logger.Debugf("reportArtifactName for push %s\n", reportArtifactName)
	common.Logger.Debugf("reportFullRepoName for push %s\n", reportFullRepoName)
	cmd = exec.Command("xnfrv", "push", reportArtifactName, reportFullRepoName)
	output, err = cmd.CombinedOutput()
	if err != nil {
		common.Logger.Errorf("Cannot push report: %s -> %s\n", err.Error(), string(output))
		return err
	}
	common.Logger.Debug(string(output))

	// TODO: Create report layer and insert to repo instead
	return nil
}

func (l *RedisListener) listenAndBlock() {
	// Run worker server.
	if err := l.r.Listener.Run(l.r.Mux); err != nil {
		common.Logger.Fatal(err)
	}
}

func CreateAndRunListener() {
	listener, err := NewListener()
	if err != nil {
		common.Logger.Fatal("Unable to start the Redis listeners")
	}
	listener.listenAndBlock()
}
