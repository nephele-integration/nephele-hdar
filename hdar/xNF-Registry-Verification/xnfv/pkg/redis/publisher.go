package redis

import (
	"encoding/json"

	"github.com/hibiken/asynq"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfv/pkg/models"
)

// Client Publisher
type RedisPublisher struct {
	r *RedisConnection
}

func NewPublisher() *RedisPublisher {
	r, e := NewRedisConnection()
	if e != nil {
		common.Logger.Error(e.Error())
		return nil
	}
	return &RedisPublisher{
		r: r,
	}
}

func (p *RedisPublisher) PublishPushEvent(e models.Event) error {
	// Connect & Disconnect
	p.r.InitPublisher()
	defer p.r.Publisher.Close()

	// Generate JSON
	payload, err := json.Marshal(e)
	if err != nil {
		common.Logger.Errorf(err.Error())
		return err
	}
	// TODO: Check if same image already with action.
	// If request user agent is xnfv discard Cancel and rerun
	task := asynq.NewTask(PushEvent, payload)

	// Request to process the task immediately in critical queue.
	_, err = p.r.Publisher.Enqueue(
		task,                    // task payload
		asynq.Queue("critical"), // set queue for task
	)
	if err != nil {
		common.Logger.Errorf(err.Error())
		return err
	}
	return nil
}
