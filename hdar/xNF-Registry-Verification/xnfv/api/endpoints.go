package api

// Use https://github.com/swaggo/swag#declarative-comments-format to format REST API

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfv/pkg/endpoints"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfv/pkg/models"
)

// Health godoc
// @Summary Health check of the service
// @Description Provides start checks for K8s
// @Produce  plain
// @Router /health [get]
func Health(w http.ResponseWriter, r *http.Request) {
	common.Logger.Info("Requested a heath check")
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprintf(w, "Status OK")
}

// notify godoc
// @Summary Webhook for notifications
// @Description Endpoint triggered by events over the xNF Registry
// @Accept  json
// @Produce  plain
// @Param notification body models.Notification true "Notification payload from an OCI Registry"
// @Router /notify [post]
func Notify(w http.ResponseWriter, r *http.Request) {
	common.Logger.Info("Received a notification")
	var notification models.Notification
	err := json.NewDecoder(r.Body).Decode(&notification)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	b, err := json.Marshal(notification.Events[0])
	if err != nil {
		common.Logger.Errorf("Error processing input: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Internal Error reading event")
		return
	}
	common.Logger.Debugf("Received %+v\n", string(b))
	// Run the scans/verification in the background here
	returnCode := endpoints.ProcessEvents(&notification)
	w.WriteHeader(returnCode)
	fmt.Fprintf(w, "Notification processed")
}

// GetReport godoc
// @Summary Verification Report
// @Description Obtain the Verification Report for an artifact
// @Accept  plain
// @Produce  application/yaml
// @Param artifact query string true "Full name of the artifact e.g., myartifact:1.0.0, someproject/myartifact:1.0.0"
// @Router /vreport [get]
func GetReport(w http.ResponseWriter, r *http.Request) {
	common.Logger.Info("Received a request")
	// Obtain the name from the path
	artifact := r.URL.Query().Get("artifact")
	// Check if 'artifact' parameter is not empty
	if artifact == "" {
		http.Error(w, "Artifact parameter is required", http.StatusBadRequest)
		return
	}
	returnCode, data := endpoints.GetReportData(artifact)
	w.WriteHeader(returnCode)
	w.Header().Set("Content-Type", "application/yaml")
	_, _ = w.Write(data)
}
