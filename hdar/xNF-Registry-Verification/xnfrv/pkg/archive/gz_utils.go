package archive

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

func WriteToTar(out *tar.Writer, name string, body []byte) error {
	h := &tar.Header{
		Name:    filepath.ToSlash(name),
		Mode:    0644,
		Size:    int64(len(body)),
		ModTime: time.Now(),
	}
	if err := out.WriteHeader(h); err != nil {
		return err
	}
	_, err := out.Write(body)
	return err
}

func EnsureGZFormat(a Archive) error {
	if !strings.HasSuffix(a.FileRaw.Name, ".tgz") && !strings.HasSuffix(a.FileRaw.Name, ".tar.gz") {
		return errors.New("Cannot load a file that is not a gz tarball")
	} else if contentType := http.DetectContentType(a.FileRaw.Data); contentType != "application/x-gzip" {
		return fmt.Errorf("%s does not seem to be a tar.gz file. Has contentType %s", a.FileRaw.Name, contentType)
	}
	return nil
}

func WriteGZFile(name, origin, destination string) error {
	// Prepare gz+tar writer to file
	if destination != "." {
		os.MkdirAll(destination, 0777)
	}
	tmpF, err := os.Create(filepath.Join(destination, name))
	if err != nil {
		tmpF.Close()
		os.Remove(filepath.Join(destination, name))
		DeleteDirIfEmpty(destination)
		return err
	}
	zipper := gzip.NewWriter(tmpF)
	tw := tar.NewWriter(zipper)
	defer func() {
		tw.Close()
		zipper.Close()
		tmpF.Close()
	}()
	// Save contents in tarball
	f, err := os.Stat(origin)
	if err != nil {
		tmpF.Close()
		os.Remove(filepath.Join(destination, name))
		DeleteDirIfEmpty(destination)
		return err
	} else if !f.IsDir() {
		tmpF.Close()
		os.Remove(filepath.Join(destination, name))
		DeleteDirIfEmpty(destination)
		return errors.New("Cannot tar a file without a parent directory.")
	} else {
		err = filepath.Walk(origin,
			func(fullPath string, info os.FileInfo, err error) error {
				if info, _ := os.Stat(fullPath); info.IsDir() {
					// ignore the folders
					return nil
				}
				if err != nil {
					return err
				}
				raw, err := ioutil.ReadFile(fullPath)
				if err != nil && err != io.EOF {
					return fmt.Errorf("File '%s' cannot be read: %s", fullPath, err)
				}
				common.Logger.Debugf("Preparing to package %s\n", fullPath)
				unwantedParentDir, base := filepath.Split(origin)
				fullParent, _ := filepath.Split(fullPath)
				common.Logger.Debugf("Parents %s, %s\n", unwantedParentDir, fullParent)
				wantedParentDir := base
				wantedParentDir = strings.TrimPrefix(fullParent, unwantedParentDir)
				wantedParentDir = strings.TrimPrefix(wantedParentDir, "/")
				common.Logger.Debugf("wanted Parent %s\n", wantedParentDir)
				innerPath := filepath.Join(wantedParentDir, info.Name())
				common.Logger.Debugf("Writing %s\n", innerPath)
				return WriteToTar(tw, innerPath, raw)
			})
		if err != nil {
			tmpF.Close()
			os.Remove(filepath.Join(destination, name))
			DeleteDirIfEmpty(destination)
			return err
		}
	}
	return nil
}

// should probably depend on a struct and be private
func UnGzip(a *Archive, target string) error {
	// Prepare gz+tar reader from file
	for _, fRaw := range a.ContentRaw {
		os.MkdirAll(filepath.Join(target, fRaw.Dir), 0777)
		if fRaw.Name == "" {
			common.Logger.Debugf("Empty entry in ContentRaw when unGzipping %+v\n", fRaw)
			continue
		}
		if err := os.WriteFile(filepath.Join(target, fRaw.Dir, fRaw.Name), fRaw.Data, 0644); err != nil {
			return err
		}
	}
	return nil
}
