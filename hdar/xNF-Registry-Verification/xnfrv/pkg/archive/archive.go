package archive

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

/*
Utilities for artifact files in local storage.
TODO: Implement interface approach for zip ?
*/

// Works with an in-memory tar.gz archive to generate the OCI specs
type Archive struct {
	FileRaw FileRaw
	// Right now we only save the descriptor file (.yaml or .yml)
	ContentRaw []FileRaw
}

type FileRaw struct {
	Name string
	Dir  string
	Data []byte
}

// TODO: Should have a clean memory utility to free space

// Prepares the struct instance to be used. Does not load Content since it requires to know the descriptor type
func NewBaselineArchive(path string) (*Archive, error) {
	a := Archive{}
	f, err := os.Stat(path)
	var filePath string
	if err != nil {
		return nil, err
	} else if f.IsDir() {
		// We always work from local archives, not directories
		return nil, errors.New("Needs a tar.gz file. Please use command 'package tar <PATH<' first.")
	} else {
		// Default case, we work with a gz tarball
		filePath = path
	}

	// Prepare global layer of the archive
	raw, err := ioutil.ReadFile(filePath)
	if err != nil && err != io.EOF {
		return nil, fmt.Errorf("File '%s' cannot be read: %s", filePath, err)
	}
	dir, fileName := filepath.Split(filePath)
	a.FileRaw = FileRaw{
		Name: fileName,
		Dir:  dir,
		Data: raw,
	}
	if err = EnsureGZFormat(a); err != nil {
		return nil, err
	}
	return &a, nil
}

func NewEmptyArchive() *Archive {
	emptyFileRaw := FileRaw{
		Data: make([]byte, 0),
		Dir:  "",
		Name: "",
	}
	return &Archive{
		FileRaw:    emptyFileRaw,
		ContentRaw: []FileRaw{emptyFileRaw},
	}
}
