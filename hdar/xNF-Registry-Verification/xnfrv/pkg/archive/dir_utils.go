package archive

import (
	"io/ioutil"
	"os"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

func DeleteDirIfEmpty(path string) error {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	if len(files) != 0 {
		return nil
	}

	err = os.Remove(path)
	if err != nil {
		return err
	}
	return nil
}

func CreateDirIfNotExist(path string) error {
	// Check if the directory exists and create it if necessary
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		err := os.MkdirAll(path, 0777)
		if err != nil {
			common.Logger.Infof("Unable to create the desired destination path: %s\n", err.Error())
			return err
		}
	} else if err != nil {
		common.Logger.Infof("Unable to process given destination path: %s\n", err.Error())
		return err
	}
	return nil
}
