package archive

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

// Wrapper function to duplicate a file with a different name/location and possibly delete the original one
func RenameFile(src string, dst string, del bool) error {
	input, err := ioutil.ReadFile(src)
	if err != nil {
		common.Logger.Infof("Unable to read file %s\n", err.Error())
		return err
	}
	baseDir := filepath.Dir(dst)
	err = CreateDirIfNotExist(baseDir)
	if err != nil {
		common.Logger.Infof("Error creating destination dir %s\n", err.Error())
		return err
	}
	err = ioutil.WriteFile(dst, input, 0644)
	if err != nil {
		common.Logger.Infof("Error creating destination file %s\n", err.Error())
		return err
	}
	if del {
		common.Logger.Infof("Deleting %s\n", src)
		if err := os.Remove(src); err != nil {
			common.Logger.Infof("Error deleting original file %s\n", err.Error())
			return err
		}
	}
	return nil
}
