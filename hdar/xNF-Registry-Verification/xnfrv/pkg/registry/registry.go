package registry

import (
	"context"
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	credentials "github.com/oras-project/oras-credentials-go"
	"oras.land/oras-go/v2"
	"oras.land/oras-go/v2/content"
	"oras.land/oras-go/v2/registry/remote"
	"oras.land/oras-go/v2/registry/remote/auth"
	"oras.land/oras-go/v2/registry/remote/retry"
)

func NewRepoWithExistingDockerCredentials(url string) (*remote.Repository, error) {
	// Used to access specific repos in a registry, auth should be in docker already
	repo, err := newRepoWithOptions(url)
	if err != nil {
		return nil, err
	}
	auth, err := newAuthenticatorFromDocker()
	if err != nil {
		return nil, err
	}
	repo.Client = auth
	return repo, nil
}

func NewRegWithExistingDockerCredentials(url string) (*remote.Registry, error) {
	// Used to access registry-level APIs (list cmd). auth should be in docker already
	reg, err := newRegWithOptions(url)
	if err != nil {
		return nil, err
	}
	auth, err := newAuthenticatorFromDocker()
	if err != nil {
		return nil, err
	}
	reg.Client = auth
	return reg, nil

}

func NewRegWithStaticCredentials(url, username, password string) (*remote.Registry, error) {
	// Only way to add new credentials. User may do docker/helm login instead
	reg, err := newRegWithOptions(url)
	if err != nil {
		return nil, err
	}
	// by default uses credStore of docker during credentials.Login
	a, err := newAuthenticatorStatic(reg.Reference.Reference, username, password)
	if err != nil {
		common.Logger.Info("Failed here")
		return nil, err
	}
	reg.Client = a
	
	ctx := context.Background()
	// Login returns ErrPlaintextPutDisabled if native (from docker engine) store is
	// not available and StoreOptions.AllowPlaintextPut is set to false.
	storeOpts := credentials.StoreOptions{
		AllowPlaintextPut: true,
	}
	credStore, err := credentials.NewStoreFromDocker(storeOpts)
	if err != nil {
		common.Logger.Errorf("Unable to load docker cred store %s\n", err.Error())
		return nil, err
	}
	creds := auth.Credential{
		Username: username,
		Password: password,
	}

	if err = credentials.Login(ctx, credStore, reg, creds); err != nil {
		common.Logger.Errorf("Username or Password incorrect %s\n", err.Error())
		return nil, err
	}

	return reg, nil

}

func newAuthenticatorFromDocker() (*auth.Client, error) {
	// We will be using the docker approach for storing credentials
	// ToDo: might need to support login without docker
	storeOpts := credentials.StoreOptions{}
	credStore, err := credentials.NewStoreFromDocker(storeOpts)
	if err != nil {
		return nil, err
	}
	// Prepare the auth client for the registry and credential store
	return &auth.Client{
		Client:     retry.DefaultClient,
		Cache:      auth.DefaultCache,
		Credential: credentials.Credential(credStore), // Use the credential store
	}, nil

}
func newAuthenticatorStatic(url, u, p string) (*auth.Client, error) {
	// Prepare the auth client for the registry/repository and credential store
	return &auth.Client{
		Client: retry.DefaultClient,
		Cache:  auth.DefaultCache,
		Credential: auth.StaticCredential(url, auth.Credential{
			Username: u,
			Password: p,
		}),
	}, nil

}

func newRegWithOptions(url string) (*remote.Registry, error) {
	ref := url
	PlainHTTP := false
	if strings.HasPrefix(url, "http://") {
		ref = strings.TrimPrefix(url, "http://")
		PlainHTTP = true
	}
	common.Logger.Infof("ref: %s", ref)
	reg, err := remote.NewRegistry(ref)
	if err != nil {
		return nil, err
	}
	reg.PlainHTTP = PlainHTTP
	return reg, err
}

func newRepoWithOptions(url string) (*remote.Repository, error) {
	ref := url
	PlainHTTP := false
	if strings.HasPrefix(url, "http://") {
		ref = strings.TrimPrefix(url, "http://")
		PlainHTTP = true
	}
	repo, err := remote.NewRepository(ref)
	if err != nil {
		return nil, err
	}
	repo.PlainHTTP = PlainHTTP
	if repo.Reference.Reference == "" {
		// TODO: Find the newest one from all tags when used for pull
		t := "latest"
		ctx := context.Background()
		err = repo.Tags(ctx, "", func(tags []string) error {
			for _, tag := range tags {
				// TODO check order and get latest
				t = tag
				break
			}
			return nil
		})
		if err != nil {
			common.Logger.Infof("Unable to access tags %s, using latest\n", err.Error())
			err = nil
		}
		repo.Reference.Reference = t
	}
	return repo, err
}

// An artifact is defined by an Image manifest and a Config Manifest
// Each manifest has a descriptor that defines its storage
type ArtifactImageSpec struct {
	ImageDescriptor       v1.Descriptor
	ImageManifestContent  []byte
	ImageManifest         v1.Manifest
	ConfigDescriptor      v1.Descriptor
	ConfigManifestContent []byte
}

func FetchManifest(repo *remote.Repository) (*ArtifactImageSpec, error) {
	ctx := context.Background()
	// fetch image descriptor only (info about target content -image manifest-)
	imageDesc, err := repo.Resolve(ctx, repo.Reference.Reference)
	if err != nil {
		common.Logger.Errorf("Unable pull image descriptor: %s\n", err.Error())
		return &ArtifactImageSpec{}, err
	}
	// fetch image manifest content
	imageManifestContent, err := content.FetchAll(ctx, repo, imageDesc)

	if err != nil {
		common.Logger.Errorf("Unable to pull image content: %s\n", err.Error())
		return &ArtifactImageSpec{}, err
	}
	// unmarshal manifest content to extract config descriptor
	var imageManifest v1.Manifest
	if err := json.Unmarshal(imageManifestContent, &imageManifest); err != nil {
		return &ArtifactImageSpec{}, err
	}
	common.Logger.Infof("Manifest %+v\n", imageManifest)
	// fetch config manifest content
	configContent, err := content.FetchAll(ctx, repo, imageManifest.Config)
	if err != nil {
		common.Logger.Errorf("Unable to pull config content: %s\n", err.Error())
		return &ArtifactImageSpec{}, err
	}
	return &ArtifactImageSpec{
		ImageDescriptor:       imageDesc,
		ImageManifestContent:  imageManifestContent,
		ImageManifest:         imageManifest,
		ConfigDescriptor:      imageManifest.Config,
		ConfigManifestContent: configContent,
	}, nil
}

func List(reg *remote.Registry) (map[string]string, error) {
	// TODO: Allow registry repo filtering
	contentMap := map[string]string{}
	ctx := context.Background()
	common.Logger.Infof("Checking repos in %+v\n", reg.Reference.Registry)
	// "Last" option not working
	err := reg.Repositories(ctx, "", func(repos []string) error {
		contentMap = make(map[string]string, len(repos))
		common.Logger.Debugf("Analyzing %d different artifacts. Might take a while\n", len(repos))
		for _, repoName := range repos {
			url := fmt.Sprintf("%s/%s", reg.Reference.Registry, repoName)
			if reg.PlainHTTP {
				url = fmt.Sprintf("http://%s", url)
			} else {
				url = fmt.Sprintf("https://%s", url)
			}
			// Need a proper tag to access the artifact. Right now it is trying to get the lastest
			repo, err := NewRepoWithExistingDockerCredentials(url)
			if err != nil {
				common.Logger.Infof("Unable to create Repo %s: %s\n", url, err.Error())
				return err
			}
			// Access the artifact info
			manifests, err := FetchManifest(repo)
			if err != nil {
				common.Logger.Infof("Unable to access artifact credentials %+v: %s\n", repo.Reference, err.Error())
				return err
			}
			// Single layer artifact support for now
			mediaType := manifests.ConfigDescriptor.MediaType
			contentMap[url] = mediaType

		}
		return nil
	})
	if err != nil {
		common.Logger.Infof("Unable to access artifacts %s\n", err.Error())
		return contentMap, err
	}
	if len(contentMap) == 0 {
		common.Logger.Info("Not found any repository matching description.")
		return contentMap, nil
	}
	// Order by repo
	orderedRepos := make([]string, 0)
	orderedContentMap := map[string]string{}
	for k := range contentMap {
		orderedRepos = append(orderedRepos, k)
	}
	sort.Strings(orderedRepos)
	for _, k := range orderedRepos {
		orderedContentMap[k] = contentMap[k]
	}
	return orderedContentMap, nil
}

func Pull(repo *remote.Repository, fs oras.Target) (*ArtifactImageSpec, error) {
	ctx := context.Background()
	// Copy from the remote repository to the destination
	_, err := oras.Copy(ctx, repo, repo.Reference.Reference, fs, repo.Reference.Reference, oras.DefaultCopyOptions)
	if err != nil {
		common.Logger.Errorf("Unable to copy artifact from remote: %s\n", err.Error())
		return &ArtifactImageSpec{}, err
	}
	common.Logger.Info("Pull successful\n")
	// There is no direct way to now the name of the file. Bring the manifest content
	manifests, err := FetchManifest(repo)
	if err != nil {
		common.Logger.Errorf("Unable to get manifests: %s\n", err.Error())
		return &ArtifactImageSpec{}, err
	}
	return manifests, nil
}
