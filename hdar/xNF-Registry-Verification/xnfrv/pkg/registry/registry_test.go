// Keep as a different package to enforce testing as a consumer of the lib
package registry_test

import (
	"os"
	"testing"

	// Installed as a consumer
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/registry"
	"oras.land/oras-go/v2/content/file"
)

func TestArtifactNameSupport(t *testing.T) {
	// Table-Driven testing
	data := []struct {
		input        string
		expectedReg  string
		expectedRepo string
		expectedRef  string
	}{
		{input: "demo.goharbor.io/demoprivate/test:latest", expectedReg: "demo.goharbor.io", expectedRepo: "demoprivate/test", expectedRef: "latest"},
		{input: "registry:5000/myimage:1.2.2", expectedReg: "registry:5000", expectedRepo: "myimage", expectedRef: "1.2.2"},
		{input: "registry:5000/myimage", expectedReg: "registry:5000", expectedRepo: "myimage", expectedRef: "latest"},
	}
	for _, val := range data {
		repo, err := registry.NewRepoWithExistingDockerCredentials(val.input)
		if err != nil {
			t.Errorf("Unable to create repo %s\n", err.Error())
		}
		if repo.Reference.Registry != val.expectedReg {
			t.Errorf("Invalid result '%s' when using %+v", repo.Reference.Registry, val)
		}
		if repo.Reference.Repository != val.expectedRepo {
			t.Errorf("Invalid result '%s' when using %+v", repo.Reference.Repository, val)
		}
		if repo.Reference.Reference != val.expectedRef {
			t.Errorf("Invalid result '%s' when using %+v", repo.Reference.Reference, val)
		}
	}

}

// Only run wih actual registry available
func TestIntegrationPullArtifact(t *testing.T) {
	// SKIPPING ALWAYS
	if true {
		return
	}
	repo, err := registry.NewRepoWithExistingDockerCredentials("http://localhost:5000/openldap_knf:1.0")
	defer os.Remove("openldap_knf-1.0.tar.gz")
	if err != nil {
		t.Errorf("Unable to create repo %s\n", err.Error())
		return
	}
	fs, err := file.New(".")
	if err != nil {
		t.Errorf("Unable to create file store to pull artifact: %s\n", err.Error())
		return
	}
	defer fs.Close()
	manifests, err := registry.Pull(repo, fs)
	if err != nil || manifests.ImageManifest.Layers[0].Annotations["org.opencontainers.image.title"] != "openldap_knf-1.0.tar.gz" {
		t.Errorf("Unable to create repo %s\n", err.Error())
		return
	}
}
