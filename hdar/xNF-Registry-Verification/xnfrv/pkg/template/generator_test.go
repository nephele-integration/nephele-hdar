package template_test

import (
	"os"
	"testing"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/template"
)

func TestGenerateArtifacts(t *testing.T) {
	// Make sure no local artifact is left
	// Table-Driven testing
	data := []struct {
		input    string
		errorMsg string
		created  bool
	}{
		{input: artifact.OSMArtifactInspector{}.GetTemplateMap()["VNF"], errorMsg: "should have located template file correctly", created: true},
		{input: "", errorMsg: "should not work if template content not given", created: false}}
	defer os.RemoveAll("unique-id-knf")
	for _, val := range data {
		created := template.GenerateLocalArtifact(".", val.input)
		if created != val.created {
			t.Errorf("%t vs %t: %s\n", created, val.created, val.errorMsg)
		}
		if created {
			_, err := os.Stat("unique-id-knf")
			if err != nil {
				t.Error("Should not have created the base dir for artifact")
			}
		}
	}

}
