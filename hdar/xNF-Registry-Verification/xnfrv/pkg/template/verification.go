package template

// While APIs and input models are expressed as Swagger specs via struct annotations,
// the IM of the descriptors and outputs of the system are expressed as json schema files

import (
	"github.com/xeipuuv/gojsonschema"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

func VerifyAgainstSchema(schema, document string) ([]string, error) {
	// Verifies syntax the document with the given schema and
	// returns a list of the schema errors found, and potential internal errors
	var schemaErrors []string
	schemaLoader := gojsonschema.NewStringLoader(schema)
	documentLoader := gojsonschema.NewStringLoader(document)

	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return schemaErrors, err
	}

	if result.Valid() {
		common.Logger.Infof("The document is valid\n")
		return schemaErrors, nil
	}
	common.Logger.Warning("The document is not valid. see errors :\n")
	for _, desc := range result.Errors() {
		common.Logger.Warningf("- %s\n", desc.String())
		schemaErrors = append(schemaErrors, desc.String())
	}
	return schemaErrors, nil
}
