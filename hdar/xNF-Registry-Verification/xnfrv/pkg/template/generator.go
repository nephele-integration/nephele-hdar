package template

// Scaffolding based on https://pkg.go.dev/gitlab.com/metakeule/scaffold/lib/scaffold

import (
	"gitlab.com/metakeule/scaffold/lib/scaffold"
	"os"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

func GenerateLocalArtifact(baseDir, templateContent string) bool {
	// TODO: enable a way to use a custom input
	defaultInput, template := scaffold.SplitTemplate(templateContent)
	if defaultInput == "" {
		common.Logger.Error("Template file should contain the default input as a json and the template instructions separated by an empty line\n%s\n", templateContent)
		return false
	}
	err := scaffold.Run(baseDir, template, strings.NewReader(defaultInput), os.Stdout, false)
	if err != nil {
		common.Logger.Errorf("Unable to generate artifact: %s\n", err.Error())
		return false
	}
	return true
}
