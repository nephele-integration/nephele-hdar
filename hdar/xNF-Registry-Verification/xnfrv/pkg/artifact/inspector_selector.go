package artifact

import (
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
)

// try-n-error to find the artifact's required Inspector
func AddCorrectInspector(a *Artifact, verify bool) bool {
	// Check if it is a OSM artifact
	ai := NewOSMArtifactInspector()
	// this internally validates the file-name to descriptor-content name and version naming convention
	err := a.LoadWithInspector(ai, verify)
	if err != nil {
		// not the right inspector
	} else {
		common.Logger.Infof("Found that the descriptor is type %s\n", a.DescriptorType)
		return true
	}
	// Check if it is a Verification Report artifact
	vReport_ai := NewVReportArtifactInspector()
	err = a.LoadWithInspector(vReport_ai, verify)
	if err != nil {
		// not the right inspector
		common.Logger.Info("Not the right inspector\n")
	} else {
		common.Logger.Infof("Found that the descriptor is type %s\n", a.DescriptorType)
		return true
	}
	common.Logger.Warningf("Unknown type of artifact %+v: %s\n", a, err.Error())
	return false

}
