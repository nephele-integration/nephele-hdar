// Keep as a different package to enforce testing as a consumer of the lib
package artifact_test

import (
	"testing"

	// Installed as a consumer
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
)

func TestInferType(t *testing.T) {

	filepath1 := "../../resources/openldap-knf-1.0.tar.gz"
	a1, err := artifact.NewLocalArtifact(filepath1)
	if err != nil {
		t.Error(err.Error())
	}
	ai1 := artifact.NewOSMArtifactInspector()
	err = a1.LoadWithInspector(ai1, true)
	if err != nil {
		t.Errorf("Unable to load descriptor with inspector provided.: %s\n", err.Error())
	}
	mediaType1 := a1.DescriptorType
	if mediaType1 != "VNF" {
		t.Errorf("Unable to detect the type for VNF %s\n", mediaType1)
	}
	filepath2 := "../../resources/openldap-ns-1.0.0.tar.gz"
	a2, err := artifact.NewLocalArtifact(filepath2)
	if err != nil {
		t.Error(err.Error())
	}
	ai2 := artifact.NewOSMArtifactInspector()
	err = a2.LoadWithInspector(ai2, true)
	if err != nil {
		t.Errorf("Unable to load descriptor with inspector provided.: %s\n", err.Error())
	}
	mediaType2 := a2.DescriptorType
	if mediaType2 != "NS" {
		t.Errorf("Unable to detect the type for NS %s\n", mediaType2)
	}
}

// Needs the artifact inspector to detect that it is not valid
func TestInvalidTarball(t *testing.T) {
	// This artifact was created manually with no parent folder
	filepath := "../../resources/invalidstructure.tar.gz"
	a, err := artifact.NewLocalArtifact(filepath)
	if err != nil {
		t.Error(err.Error())
	}
	ai := artifact.NewOSMArtifactInspector()
	err = a.LoadWithInspector(ai, false) // we want to focus on the internal structure error
	if err == nil {
		t.Error("The artifact should have been discarded")
	}
}
