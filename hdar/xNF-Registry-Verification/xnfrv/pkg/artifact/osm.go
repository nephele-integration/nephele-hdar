package artifact

import (
	"embed"
	"errors"
	"path/filepath"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/descriptor"
)

// To embed files in the executable (schema and templates)
//
//go:embed docs/osm*
var nfvFiles embed.FS

var MediaTypeNFVConfig = "application/vnd.etsi.nfv.config.v1+json"

/*
This is fully customizable and should allow us to implement adaptations for OSM, TOSCA and others.
Each case should obtain the info from the corresponding descriptor.
*/
type NFVConfig struct {
	// SchemaVersion to xNFRV version: 1 = [v0.3.0, ]. Mandatory in all implementations
	SchemaVersion string `json:"schemaVersion"`
	// Config MediaType. For all ETSI NFV CSARs. Mandatory in all implementations
	MediaType string `json:"mediaType"`
	// Reference to the specification of the CSAR
	PackageSpec string `json:"packageSpec"`
	// Reference to the specification of the content's structure
	DataModelSpec string `json:"dataModelSpec"`
	// Path to the core descriptor file
	DescriptorFile string `json:"descriptorFile"`
	// NFV Level described in the descriptor. Mandatory in all implementations and needs to match GetTemplateMap keys
	DescriptorType string `json:"descriptorType"`
	// Unique identifier of the descriptor for the same descriptorType
	DescriptorId string `json:"descriptorId"`
	// Version of the descriptor
	DescriptorVersion string `json:"descriptorVersion"`
	// Dependencies of the descriptor
	Dependencies []descriptor.Dependency `json:"dependencies"`
}

// Use GetDescriptorType to find out if it is the expected Inspector implementation
type OSMArtifactInspector struct {
}

func NewOSMArtifactInspector() OSMArtifactInspector {
	return OSMArtifactInspector{}
}

func (ai OSMArtifactInspector) GetDescriptorInspector(a *Artifact) descriptor.DescriptorInspector {
	i := &descriptor.OSMDescriptorInspector{
		Raw: a.GetDescriptorFileRaw().Data,
	}
	err := i.LoadContent()
	if err != nil {
		return nil
	}
	return i
}

func (ai OSMArtifactInspector) GetConfig(a *Artifact) interface{} {
	// TODO: inspect the artifact when we support more than OSM SOL004 and SOL006
	// e.g. TOSCA, TOSCA+metadata, +signature etc
	config := &NFVConfig{
		SchemaVersion: "1",
		PackageSpec:   "SOL004",
		DataModelSpec: "SOL006",
		MediaType:     ai.GetMediaType(),
		Dependencies:  a.Dependencies,
	}
	dInspector := ai.GetDescriptorInspector(a)
	dRaw := a.GetDescriptorFileRaw()
	config.DescriptorFile = filepath.Join(dRaw.Dir, dRaw.Name)
	config.DescriptorType = dInspector.GetDescriptorType()
	config.DescriptorId = dInspector.GetUniqueName().Value
	config.DescriptorVersion = dInspector.GetVersion().Value
	return config
}

func (ai OSMArtifactInspector) GetMediaType() string {
	return MediaTypeNFVConfig
}

func (ai OSMArtifactInspector) IsDescriptorFileSuffix(fName string) bool {
	return strings.HasSuffix(fName, ".yaml") || strings.HasSuffix(fName, ".yml")
}

func (ai OSMArtifactInspector) VerifyStructure(arc *archive.Archive) error {
	// Only one Yaml or yml file allowed in the base folder
	ok := false
	for _, fRaw := range arc.ContentRaw {
		subDir, _ := filepath.Split(filepath.Dir(fRaw.Dir))
		if subDir != "" || !ai.IsDescriptorFileSuffix(fRaw.Name) {
			// This is not the core descriptor since it is in a subfolder
			// Or does not have the right suffix
			continue
		}
		if ok {
			common.Logger.Warning("More than one Yaml file was found in base folder")
			ok = false
			break
		}
		ok = true
	}
	if !ok {
		return errors.New("More than one descriptor in base folder")
	}
	return nil
}

func (ai OSMArtifactInspector) GetSchemaMap() map[string]string {
	// For OSM descriptors the test is not representative, we would need to
	// use the actual osm CLI since we do not want to host the complete IM schema (in yang...)
	knfData, _ := nfvFiles.ReadFile("docs/osm_knf_schema.json")
	nsData, _ := nfvFiles.ReadFile("docs/osm_ns_schema.json")
	return map[string]string{
		"VNF": string(knfData),
		"NS":  string(nsData),
	}
}

func (ai OSMArtifactInspector) GetTemplateMap() map[string]string {
	// Provides a baseline archive (folder format) of the artifact
	knfData, _ := nfvFiles.ReadFile("docs/osm_knf.templ")
	nsData, _ := nfvFiles.ReadFile("docs/osm_ns.templ")
	return map[string]string{
		"VNF": string(knfData),
		"NS":  string(nsData),
	}
}
