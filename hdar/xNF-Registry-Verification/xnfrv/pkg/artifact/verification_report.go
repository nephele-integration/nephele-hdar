package artifact

import (
	"embed"
	"errors"
	"path/filepath"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/descriptor"
)

//go:embed docs/veri*
var vReportFiles embed.FS

var MediaTypeVReportConfig = "application/vnd.eviden.verification.config.v1+json"

type VReportConfig struct {
	// SchemaVersion to xNFRV version: 1 = [v0.1.0, ]. Mandatory in all implementations
	SchemaVersion string `json:"schemaVersion"`
	// Config MediaType. Mandatory in all implementations
	MediaType string `json:"mediaType"`
	// Path to the core descriptor file
	DescriptorFile string `json:"descriptorFile"`
	// Needs to match GetTemplateMap keys
	DescriptorType string `json:"descriptorType"`
	// Unique identifier of the descriptor for the same descriptorType
	DescriptorId string `json:"descriptorId"`
	// Version of the descriptor
	DescriptorVersion string `json:"descriptorVersion"`
}

// Use GetDescriptorType to find out if it is the expected Inspector implementation
type VReportArtifactInspector struct {
}

func NewVReportArtifactInspector() VReportArtifactInspector {
	return VReportArtifactInspector{}
}

func (ai VReportArtifactInspector) GetDescriptorInspector(a *Artifact) descriptor.DescriptorInspector {
	i := &descriptor.VReportDescriptorInspector{
		Raw: a.GetDescriptorFileRaw().Data,
	}
	err := i.LoadContent()
	if err != nil {
		return nil
	}
	return i
}

func (ai VReportArtifactInspector) GetConfig(a *Artifact) interface{} {
	config := &VReportConfig{
		SchemaVersion:  "0.1.0",
		MediaType:      ai.GetMediaType(),
		DescriptorType: "VREPORT",
	}
	dInspector := ai.GetDescriptorInspector(a)
	dRaw := a.GetDescriptorFileRaw()
	config.DescriptorFile = filepath.Join(dRaw.Dir, dRaw.Name)
	config.DescriptorType = dInspector.GetDescriptorType()
	config.DescriptorId = dInspector.GetUniqueName().Value
	config.DescriptorVersion = dInspector.GetVersion().Value
	return config
}

func (ai VReportArtifactInspector) GetMediaType() string {
	return MediaTypeVReportConfig
}

func (ai VReportArtifactInspector) IsDescriptorFileSuffix(fName string) bool {
	return strings.HasSuffix(fName, ".yaml") || strings.HasSuffix(fName, ".yml")
}

func (ai VReportArtifactInspector) VerifyStructure(arc *archive.Archive) error {
	// Only one Yaml or yml file allowed in the base folder
	ok := false
	for _, fRaw := range arc.ContentRaw {
		subDir, _ := filepath.Split(filepath.Dir(fRaw.Dir))
		if subDir != "" || !ai.IsDescriptorFileSuffix(fRaw.Name) {
			// This is not the core descriptor since it is in a subfolder
			// Or does not have the right suffix
			continue
		}
		if ok {
			common.Logger.Warning("More than one Yaml file was found in base folder")
			ok = false
			break
		}
		ok = true
	}
	if !ok {
		return errors.New("More than one descriptor in base folder")
	}
	return nil
}

func (ai VReportArtifactInspector) GetSchemaMap() map[string]string {
	// For VReport descriptors the test is not representative, we would need to
	// use the actual VReport CLI since we do not want to host the complete IM schema (in yang...)
	vReportData, _ := vReportFiles.ReadFile("docs/verification_report_schema.json")
	return map[string]string{
		"VREPORT": string(vReportData),
	}
}

func (ai VReportArtifactInspector) GetTemplateMap() map[string]string {
	// Provides a baseline archive (folder format) of the artifact
	vReportData, _ := vReportFiles.ReadFile("docs/verification_report.templ")
	return map[string]string{
		"VREPORT": string(vReportData),
	}
}
