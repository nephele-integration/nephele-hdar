package artifact

import (
	"archive/tar"
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"unicode"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/descriptor"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/template"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/utils"
)

var MediaTypeLayer = "application/tar+gz"

// The struct that implements this should not require any value, everything is added as parameter
type ArtifactInspector interface {
	// Helper method for LoadArchiveFiles
	IsDescriptorFileSuffix(fName string) bool
	// Returns the Config struct with the relevant info
	GetConfig(a *Artifact) interface{}
	// Returns an implementation of the DescriptorInspector interface
	GetDescriptorInspector(a *Artifact) descriptor.DescriptorInspector
	// Ensure all expected files are present (just the file format and locations)
	VerifyStructure(*archive.Archive) error
	// Provide a JSON schema path map based on the artifact type
	GetSchemaMap() map[string]string
	// Provide a template path map based on the artifact type
	GetTemplateMap() map[string]string
	// Provide config mediatype
	GetMediaType() string
}

// Baseline "Inspector". Specific methods created separately as a ArtifactInspector implementation
type Artifact struct {
	FPath           string
	InMemoryArchive *archive.Archive
	Inspector       ArtifactInspector
	ConfigContent   []byte
	DescriptorType  string
	ConfigMediaType string
	Dependencies    []descriptor.Dependency
}

// Change config info with map[string]string

func NewEmptyArtifact() *Artifact {
	return &Artifact{}
}

// Creates a baseline Artifact, needs to call LoadInspector next for full configuration
func NewLocalArtifact(fPath string) (*Artifact, error) {
	a := &Artifact{
		FPath: fPath,
	}
	// Load contents in memory
	rawContents, err := archive.NewBaselineArchive(fPath)
	if err != nil {
		common.Logger.Info("Unable to load file in memory")
		return nil, err
	}
	a.InMemoryArchive = rawContents
	return a, nil
}

func (a *Artifact) LoadWithInspector(inspector ArtifactInspector, verify bool) error {
	// The remaining is too specific to implement at the generic artifact level
	a.Inspector = inspector
	// Clean artifact
	a.InMemoryArchive.ContentRaw = nil
	// Almost generic implementation to find and load descriptor to InMemoryArchive.Content[0]
	// specific part implemented via IsDescriptorFileSuffix in ArtifactInspector implementations
	err := a.LoadArchiveFiles()
	if err != nil || len(a.InMemoryArchive.ContentRaw) == 0 {
		return fmt.Errorf("The provided inspector is not valid: Error: %s, Data Found: %+v\n", err, a.InMemoryArchive.ContentRaw)
	}
	dInspector := a.Inspector.GetDescriptorInspector(a)
	if dInspector == nil || dInspector.GetDescriptorType() == "" {
		return fmt.Errorf("Not the right Inspector")
	}
	a.Dependencies = dInspector.GetDependencies()
	a.ConfigContent, err = json.MarshalIndent(a.Inspector.GetConfig(a), "", "\t")
	a.DescriptorType = dInspector.GetDescriptorType()
	a.ConfigMediaType = a.Inspector.GetMediaType()
	if err != nil {
		return fmt.Errorf("Unable to load Config Content: %s\n", err.Error())
	}
	// This is also called from tar when using a temp tar.gz file that does not follow naming convention
	if verify {
		// Specific part implemented via DescriptorInspector
		err = a.Lint()
		if err != nil {
			common.Logger.Info("Verification failed. Check logs\n")
			return err
		}
	}
	return nil
}

func (a *Artifact) LoadArchiveFiles() error {
	// Prepare gz+tar reader from file
	fReader, err := os.Open(filepath.Join(a.InMemoryArchive.FileRaw.Dir, a.InMemoryArchive.FileRaw.Name))
	defer fReader.Close()
	if err != nil {
		return err
	}
	unzipped, err := gzip.NewReader(fReader)
	defer unzipped.Close()
	if err != nil {
		return err
	}

	tr := tar.NewReader(unzipped)
	for { // While not EOF
		hd, err := tr.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}
		// Contents need to come in a directory, check if the tarball was correctly created
		dir, fileName := filepath.Split(hd.Name)
		if dir == "" {
			// This cannot happen because we would extract contents directly on the work dir
			return fmt.Errorf("Tarball content '%s' bad formatted. Expecting a parent directory", hd.Name)
		}
		buffer := make([]byte, hd.Size)
		_, err = tr.Read(buffer)
		if err != nil && err != io.EOF {
			return fmt.Errorf("File '%s' cannot be read: %s", fileName, err)
		}
		common.Logger.Debugf("Adding '%s' '%s' to artifact inmemory\n", dir, fileName)
		a.InMemoryArchive.ContentRaw = append(a.InMemoryArchive.ContentRaw, archive.FileRaw{
			Name: fileName,
			Dir:  dir,
			Data: buffer,
		})
	}
	return nil
}

func (a *Artifact) GetDescriptorFileRaw(dNameArgs ...string) (f archive.FileRaw) {
	// dNameArgs[0] set to "" by default.
	emptyF := archive.FileRaw{}
	dName := ""
	if len(dNameArgs) > 1 {
		common.Logger.Errorf("Invalid parameters to GetDescriptorFileRaw %+v", dNameArgs)
		return emptyF
	} else if len(dNameArgs) == 1 {
		dName = dNameArgs[0]
	}

	for _, fRaw := range a.InMemoryArchive.ContentRaw {
		if dName != "" && dName == fRaw.Name {
			common.Logger.Infof("Found descriptor requested by name: %s\n", fRaw.Name)
			return fRaw
		}
		subDir, _ := filepath.Split(filepath.Dir(fRaw.Dir))
		if subDir != "" || !a.Inspector.IsDescriptorFileSuffix(fRaw.Name) {
			// This is not the core descriptor since it is in a subfolder
			// Or does not have the right suffix
			continue
		}
		return fRaw
	}
	// This may happen with tar and untar commands since we do not know their types yet
	common.Logger.Errorf("Descriptor not found\n")
	return emptyF
}

func (a *Artifact) Lint() error {
	// Global method to perform static verifications
	// Todo: Verify that dependencies exist
	err := a.VerifyFileFormat()
	if err != nil {
		common.Logger.Infof("Artifact name incorrectly formatted: %s\n", err.Error())
		return err
	}
	err = a.VerifyStructure()
	if err != nil {
		common.Logger.Infof("Artifact structure of files incorrectly formatted: %s\n", err.Error())
		return err
	}
	err = a.VerifySyntax()
	if err != nil {
		common.Logger.Infof("Artifact syntax of descriptor incorrectly formatted: %s\n", err.Error())
		return err
	}
	return nil
}

func (a *Artifact) VerifyFileFormat() error {
	// Verifies naming convention id-version.ext (tar.gz or tgz) against contents in the descriptor
	// Same for all implementations
	fileFormat, err := utils.GetFileFormat(a.FPath, ".tar.gz")
	if err != nil {
		return err
	}
	descInspector := a.Inspector.GetDescriptorInspector(a)
	if descInspector == nil {
		return errors.New("Unable to load contents of the descriptor")
	}
	rName := descInspector.GetUniqueName()
	rVersion := descInspector.GetVersion()
	if rName.Error != nil || rVersion.Error != nil {
		return fmt.Errorf("Error Name: %s, Error Version: %s", rName.Error.Error(), rVersion.Error.Error())
	}
	if rName.Value != fileFormat["name"] || rVersion.Value != fileFormat["version"] {
		return fmt.Errorf("File %s-%s and descriptor content %s-%s do not match", fileFormat["name"], fileFormat["version"], rName.Value, rVersion.Value)
	}
	// https://github.com/wujunwei/helm/blob/f0fd37d2c50f947ce056e14372a3952e83251b04/pkg/chartutil/verify_name.go#L27-L37
	for _, char := range rName.Value {
		if unicode.IsUpper(char) {
			return fmt.Errorf("Unique Names cannot contain capitalized letters (CN convention): %s --> %c", rName.Value, char)
		}
	}
	return nil
}

func (a *Artifact) VerifyStructure() error {
	// This check only looks at the pattern of files, not content
	return a.Inspector.VerifyStructure(a.InMemoryArchive)
}

func (a *Artifact) VerifySyntax() error {
	// This check only looks at the descriptor content
	// ToDo: Add support for multi-descriptor artifacts
	// Json Schema expects the descriptor content in JSON format
	di := a.Inspector.GetDescriptorInspector(a)
	dContent := di.GetDescriptorContent()
	descriptorRaw, err := json.Marshal(dContent)
	if err != nil {
		return errors.New(fmt.Sprintf("Unable to convert descriptor to json: %s\n", err.Error()))
	}
	aType := a.DescriptorType

	sContent, ok := a.Inspector.GetSchemaMap()[strings.ToUpper(aType)]
	if !ok {
		return errors.New(fmt.Sprintf("Unable to obtain path to descriptor schema from map %+v and aType %s\n", a.Inspector.GetSchemaMap(), aType))
	}

	// Proceed with the verification
	sErrors, iError := template.VerifyAgainstSchema(sContent, string(descriptorRaw))
	common.Logger.Debugf("sErrors %+v\n", sErrors)
	if iError != nil {
		return errors.New(fmt.Sprintf("Internal Error: %s\n", iError.Error()))
	} else if len(sErrors) > 0 {
		return errors.New(fmt.Sprintf("Check logs for the %d errors found\n", len(sErrors)))
	} else {
		common.Logger.Infof("No errors detected")
	}
	return nil
}

func NewTmpArtifactFromFolder(fPath string) (a *Artifact, err error) {
	// We need to infer the unique name and version to create the proper file name
	// This is a tmp file for the rest of the code to work
	aName := "tmp.tar.gz"
	err = archive.WriteGZFile(aName, fPath, ".")
	if err != nil {
		common.Logger.Infof("Unable to write to GZ temp file: %s", err.Error())
		return nil, err
	}
	// Double check that the artifact can be loaded
	a, err = NewLocalArtifact(aName)
	if err != nil {
		common.Logger.Infof(err.Error())
		return nil, err
	}
	return a, nil
}

func TmpArtifactToLocalArtifact(a *Artifact, dPath string) (string, error) {
	// Tmp artifacts has to have the right inspector already injected
	// This is generic for any implementation
	defer func() {
		os.Remove(a.FPath)
	}()
	di := a.Inspector.GetDescriptorInspector(a)
	rName := di.GetUniqueName()
	rVersion := di.GetVersion()
	if rName.Error != nil || rVersion.Error != nil {
		msg := fmt.Sprintf("Error Name: %s, Error Version: %s\n", rName.Error.Error(), rVersion.Error.Error())
		common.Logger.Errorf(msg)
		return "", errors.New(msg)
	}
	aName := filepath.Join(dPath, fmt.Sprintf("%s-%s.tar.gz", rName.Value, rVersion.Value))
	err := archive.RenameFile(a.FPath, aName, true)
	if err != nil {
		common.Logger.Errorf("Unable to write to final GZ file: %s", err.Error())
		return "", err
	}
	return aName, nil
}
