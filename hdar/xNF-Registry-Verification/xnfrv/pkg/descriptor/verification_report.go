package descriptor

import (
	"errors"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
)

// Implements the DescriptorInspector interface
type VReportDescriptorInspector struct {
	Raw     []byte
	Content archive.MapStr
}

func (di *VReportDescriptorInspector) GetDescriptorContent() archive.MapStr {
	return di.Content
}

// Helper method for artifact inspector to know if it is the right one
// Returning "" means that this is not the expected Inspector interface to use
func (di VReportDescriptorInspector) GetDescriptorType() string {
	content := string(di.Raw)
	if strings.Contains(content, "lint:") {
		return "VREPORT"
	}
	return ""
}

func (di *VReportDescriptorInspector) LoadContent() error {
	mapPointer, err := archive.LoadYamlContent(di.Raw)
	if err != nil {
		return err
	}
	di.Content = mapPointer
	return nil
}

func (di *VReportDescriptorInspector) GetAnnotations() ReturnMapStringsToString {
	annotations := make(map[string]string)
	annotations["org.opencontainers.image.description"] = "Report for xNF Verification engine"
	annotations["org.opencontainers.image.authors"] = "EVIDEN"
	// Cannot be added to Manifest annotations as it breaks the fileStore
	annotations["org.opencontainers.image.title"] = di.GetUniqueName().Value
	annotations["org.opencontainers.image.version"] = di.GetVersion().Value
	return ReturnMapStringsToString{
		Value: annotations,
		Error: nil,
	}
}

func (di *VReportDescriptorInspector) GetUniqueName() ReturnString {
	common.Logger.Debugf("%+v\n", di.Content)
	if val, ok := di.Content["id"]; ok {
		return ReturnString{
			Value: val.(string),
			Error: nil,
		}
	}
	return ReturnString{
		Value: "",
		Error: errors.New("Unable to obtain unique name from VReport descriptor\n"),
	}
}

func (di *VReportDescriptorInspector) GetVersion() ReturnString {
	return ReturnString{
		Value: "latest",
		Error: nil,
	}
}

func (di *VReportDescriptorInspector) GetDependencies() []Dependency {
	return make([]Dependency, 0)
}
