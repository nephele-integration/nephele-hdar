package descriptor

import "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"

// Interface to allow new custom oci descriptors
// The struct that implements this interface requires at least one entry:
//
//	Raw     []byte // byte content of the descriptor
//
// TODO: Might be good to keep it with no forced entry in the struct
type DescriptorInspector interface {
	GetUniqueName() ReturnString
	GetVersion() ReturnString
	GetAnnotations() ReturnMapStringsToString
	GetDescriptorContent() archive.MapStr
	GetDescriptorType() string
	// Provide a list of dependencies
	GetDependencies() []Dependency
}

type Dependency struct {
	// NFV Level described in the descriptor. Mandatory in all implementations and needs to match GetTemplateMap keys
	DescriptorType string `json:"descriptorType"`
	// Unique identifier of the descriptor for the same descriptorType
	DescriptorId string `json:"descriptorId"`
	// // Version of the descriptor
	// DescriptorVersion string `json:"descriptorVersion"`
	// Location [local, implicit, external]
	Location string `json:"location"`
	// Mode [soft, hard]
	Mode string `json:"mode"`
}

// TODO: is there a better way to return multiple types and handle errors?
type ReturnString struct {
	Value string
	Error error
}

type ReturnMapStringsToString struct {
	Value map[string]string
	Error error
}
