package descriptor

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/utils"
)

// Implements the DescriptorInspector interface
type OSMDescriptorInspector struct {
	Raw     []byte
	Content archive.MapStr
}

func (di *OSMDescriptorInspector) GetDescriptorContent() archive.MapStr {
	return di.Content
}

// Helper method for artifact inspector to know if it is the right one
// Returning "" means that this is not the expected Inspector interface to use
func (di OSMDescriptorInspector) GetDescriptorType() string {
	content := string(di.Raw)
	if strings.Contains(content, "vnfd:") || strings.Contains(content, "vnfd-catalog:") {
		return "VNF"
	} else if strings.Contains(content, "nsd:") || strings.Contains(content, "nsd-catalog:") {
		return "NS"
	}
	return ""
}

func (di *OSMDescriptorInspector) LoadContent() error {
	mapPointer, err := archive.LoadYamlContent(di.Raw)
	if err != nil {
		return err
	}
	var content archive.MapStr
	if val, ok := mapPointer["nsd"]; ok {
		if temp_val, ok := val.(archive.MapStr)["nsd"]; ok {
			common.Logger.Info("Nested nsd entry in IM\n")
			content = temp_val.([]interface{})[0].(archive.MapStr)
			common.Logger.Infof("%+v\n", content)
		} else {
			content = val.(archive.MapStr)
		}
	} else if val, ok := mapPointer["vnfd"]; ok {
		if temp_val, ok := val.(archive.MapStr)["vnfd"]; ok {
			common.Logger.Info("Nested vnfd entry in IM\n")
			content = temp_val.(archive.MapStr)
		} else {
			content = val.(archive.MapStr)
		}
	} else {
		return fmt.Errorf("Unable to process descriptor with '%s'\n", string(di.Raw))
	}
	di.Content = content
	return nil
}

func (di *OSMDescriptorInspector) GetAnnotations() ReturnMapStringsToString {
	annotations := make(map[string]string)
	if val, ok := di.Content["description"]; ok {
		annotations["org.opencontainers.image.description"] = val.(string)
	}
	if val, ok := di.Content["designer"]; ok { // NS
		annotations["org.opencontainers.image.authors"] = val.(string)
	} else if val, ok := di.Content["provider"]; ok { // VNF
		annotations["org.opencontainers.image.authors"] = val.(string)
	}
	// Cannot be added to Manifest annotations as it breaks the fileStore
	if val, ok := di.Content["id"]; ok {
		annotations["org.opencontainers.image.title"] = val.(string)
	}
	if val, ok := di.Content["version"]; ok {
		// Recover .0 if it was lost during unmarshal (1.0 -> 1), (1.1 -> 1.1)
		if strings.Contains(val.(string), ".") {
			annotations["org.opencontainers.image.version"] = val.(string)
		} else if version, err := strconv.ParseFloat(val.(string), 1); err == nil {
			if fmt.Sprintf("%.1f", version) == "0.0" {
				annotations["org.opencontainers.image.version"] = "0"
			} else {
				annotations["org.opencontainers.image.version"] = fmt.Sprintf("%.1f", version)
			}
		} else {
			common.Logger.Info("Unable to detect version\n")
		}
	}
	// TODO: Pending to figure out how to get + org.opencontainers.image.licenses
	return ReturnMapStringsToString{
		Value: annotations,
		Error: nil,
	}
}

func (di *OSMDescriptorInspector) GetUniqueName() ReturnString {
	if val, ok := di.Content["id"]; ok && val.(string) != "" {
		return ReturnString{
			Value: val.(string),
			Error: nil,
		}
	} else {
		return ReturnString{
			Value: "",
			Error: errors.New("Unable to obtain unique name from OSM descriptor\n"),
		}
	}
}

func (di *OSMDescriptorInspector) GetVersion() ReturnString {
	if val, ok := di.Content["version"]; ok {
		v, err := utils.EnsureVersionFormat(val)
		if err != nil || v == "" {
			return ReturnString{
				Value: "",
				Error: errors.New("Unable to get version in right format\n"),
			}
		}
		return ReturnString{
			Value: v,
			Error: nil,
		}
	} else {
		return ReturnString{
			Value: "",
			Error: errors.New("Unable to obtain version from OSM descriptor\n"),
		}
	}
}

func (di *OSMDescriptorInspector) GetDependencies() []Dependency {
	var dependencies []Dependency
	mapPointer, err := archive.LoadYamlContent(di.Raw)
	if err != nil {
		common.Logger.Errorf("Unable to load raw content: %s\n", err.Error())
		return dependencies
	}
	// Descriptor is a NSD
	if _, ok := mapPointer["nsd"]; ok {
		common.Logger.Debug("Is a NSD")
		// check for implicit dependencies
		if temp_val, ok := mapPointer["vnfd"]; ok {
			// There are implicit vnfd in the same descriptor
			common.Logger.Info("Implicit VNFD in NSD\n")
			for _, vnfd := range temp_val.([]archive.MapStr) {
				dId, _ := vnfd["id"]
				// dVersion, _ := vnfd["version"]
				dependencies = append(dependencies, Dependency{
					DescriptorType: "VNF",
					DescriptorId:   dId.(string),
					// DescriptorVersion: dVersion.(string),
					Location: "implicit",
					Mode:     "hard",
				})
			}
		} else {
			// There are external VNFs required for this
			common.Logger.Info("External VNFD in NSD\n")
			for _, vnfId := range di.Content["vnfd-id"].([]interface{}) {
				dependencies = append(dependencies, Dependency{
					DescriptorType: "VNF",
					DescriptorId:   vnfId.(string),
					// DescriptorVersion: "",
					Location: "external",
					Mode:     "hard",
				})
			}
		}
		// Descriptor is a VNFD
	} else if _, ok := mapPointer["vnfd"]; ok {
		common.Logger.Debug("Is a VNFD")
		if val, ok := di.Content["sw-image-desc"]; ok {
			for _, dependency := range val.([]interface{}) {
				dependencies = append(dependencies, Dependency{
					DescriptorType: "VM",
					DescriptorId:   dependency.(archive.MapStr)["image"].(string),
					// DescriptorVersion: "",
					Location: "external",
					Mode:     "soft",
				})
			}
		} else if val, ok := di.Content["kdu"]; ok {
			for _, dependency := range val.([]interface{}) {
				if _, ok := dependency.(archive.MapStr)["juju-bundle"]; ok {
					dependencies = append(dependencies, Dependency{
						DescriptorType: "JUJU",
						DescriptorId:   dependency.(archive.MapStr)["juju-bundle"].(string),
						// DescriptorVersion: "",
						Location: "external",
						Mode:     "soft",
					})
				} else if _, ok := dependency.(archive.MapStr)["helm-chart"]; ok {
					// Check if it is local or external
					cName := dependency.(archive.MapStr)["helm-chart"].(string)
					location := "external"
					mode := "soft"
					if !strings.Contains(cName, "/") {
						location = "internal"
						mode = "hard"
					}
					dependencies = append(dependencies, Dependency{
						DescriptorType: "HELM",
						DescriptorId:   dependency.(archive.MapStr)["helm-chart"].(string),
						// DescriptorVersion: "",
						Location: location,
						Mode:     mode,
					})
				} else {
					common.Logger.Errorf("Kdu dependency not known %+v\n", dependency.(archive.MapStr))
				}
			}
		} else {
			common.Logger.Errorf("Descriptor VNFD subtype not known %+v\n", di.Content)
		}
	} else {
		common.Logger.Errorf("Descriptor type not known %+v\n", di.Content)
	}
	return dependencies
}
