package main

import (
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

func main() {
	common.Logger = common.ConfigureLogger("xNFRV")
	cmd.Execute()
}
