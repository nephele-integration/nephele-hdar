package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	"github.com/spf13/cobra"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/registry"
)

var manifestCmd = &cobra.Command{
	Use:   "manifest [REGISTRY/REPOSITORY/NAME:TAG] [FLAGS]",
	Short: "Pulls an OCI manifest. ",
	Long:  `Downloads the the OCI manifests for image and config.`,
	Run:   ManifestCmdRun,
}

func ConfigureManifestCmd(c *cobra.Command) {
	c.PersistentFlags().StringP("destination", "d", ".", "Base directory where the manifest will be downloaded")
	return
}

func ManifestCmdRun(c *cobra.Command, args []string) {
	// Check input info
	if len(args) != 1 {
		common.Logger.Info("Only accepts 2 arguments [REGISTRY/REPOSITORY/NAME:TAG]\n")
		return
	}
	// Prepare destination
	fBase, err := c.Flags().GetString("destination")
	if err != nil {
		common.Logger.Error("Unable to read destination flag\n")
		common.Logger.Error(err.Error())
		return
	}
	err = archive.CreateDirIfNotExist(fBase)
	if err != nil {
		common.Logger.Error("Unable to create destination dir\n")
		common.Logger.Error(err.Error())
		return
	}
	// 1. Prepare connection to remote registry
	// Check that args is correct (repo.Reference gives the info about reg, rep and image)
	repo, err := registry.NewRepoWithExistingDockerCredentials(args[0])
	if err != nil {
		common.Logger.Error("Unable to create repo\n")
		common.Logger.Error(err.Error())
		return
	}
	common.Logger.Debug("Repo created ok")
	// 2. Pull the info
	manifests, err := registry.FetchManifest(repo)

	if err != nil {
		common.Logger.Errorf("Unable to fetch manifests %s\n", err.Error())
		return
	}

	common.Logger.Info("Pull successful\n")
	// 3. Save as local files
	fi, _ := os.Create(fmt.Sprintf("manifest-image-%s.json", strings.ReplaceAll(repo.Reference.Repository, "/", "-")))
	fc, _ := os.Create(fmt.Sprintf("manifest-config-%s.json", strings.ReplaceAll(repo.Reference.Repository, "/", "-")))
	defer fi.Close()
	defer fc.Close()
	fi.Write(manifests.ImageManifestContent)
	fc.Write(manifests.ConfigManifestContent)
	return

}
