package cmd

import (
	"github.com/spf13/cobra"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
)

var tarCmd = &cobra.Command{
	Use:   "tar [PATH]",
	Short: "Package PATH as a tar.gz file.",
	Long:  `Packages the directory pointed by PATH as a tar.gz file.`,
	Args:  cobra.ExactArgs(1),
	Run:   TarCmdRun,
}

func init() {
	packageCmd.AddCommand(tarCmd)
}

func TarCmdRun(c *cobra.Command, args []string) {
	path, dPath, err := ValidatePackageCmdInput(args, c.Flags())
	if err != nil {
		common.Logger.Infof(err.Error())
		return
	}
	tmpA, err := artifact.NewTmpArtifactFromFolder(path)
	if err != nil {
		common.Logger.Info(err.Error())
		return
	} else if !InspectorConfig.AddCorrectInspector(tmpA, false) {
		common.Logger.Errorf("Unable to add correct inspector implementation during tar\n")
		return
	}
	aName, err := artifact.TmpArtifactToLocalArtifact(tmpA, dPath)
	if err != nil {
		common.Logger.Infof(err.Error())
		return
	}
	common.Logger.Infof("Created file %s\n", aName)
	return
}
