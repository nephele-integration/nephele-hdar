package cmd

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	"github.com/spf13/cobra"
	"golang.org/x/term"
	
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/registry"
)

var loginCmd = &cobra.Command{
	Use:   "login [REGISTRY] [flags]",
	Short: "Log in to a remote registry",
	Long:  `Log in to a remote registry. Registry might be a url or ip:port. No repository is needed.`,
	Args:  cobra.ExactArgs(1),
	Run:   LoginCmdRun,
}

func ConfigureLoginCmd(c *cobra.Command) {
	// Add option registry here??
	c.PersistentFlags().StringP("username", "u", "", "username in the registry")
	c.PersistentFlags().StringP("password", "p", "", "Password or Personal Access Token")
}

func LoginCmdRun(cmd *cobra.Command, args []string) {
	// We store credentials in the standard docker store for the local environment
	p, err := cmd.Flags().GetString("password")
	u, err := cmd.Flags().GetString("username")

	if u == "" {
		// prompt for username
		u, err = readLine("Username: ", false)
		if err != nil {
			common.Logger.Infof("Unable to process input %s\n", err.Error())
			return
		}
		u = strings.TrimSpace(u)
	}
	if p == "" {
		// prompt for token
		if p, err = readLine("Password: ", true); err != nil {
			common.Logger.Infof("Unable to process input %s\n", err.Error())
			return
		}
		p = strings.TrimSpace(p)
	}

	reg, err := registry.NewRegWithStaticCredentials(args[0], u, p)
	if err != nil {
		common.Logger.Infof("Unable to access reg %+v with credentials %s\n", reg, err.Error())
		return
	}

	common.Logger.Info("Login Succeeded\n")
	return
}

// TODO Could simplify this a lot
func readLine(prompt string, silent bool) (string, error) {
	fmt.Print(prompt)
	fd := int(os.Stdin.Fd())
	var bytes []byte
	var err error
	// not working
	if silent && term.IsTerminal(fd) {
		if bytes, err = term.ReadPassword(fd); err == nil {
			_, err = fmt.Println()
		}
	} else {
		bytes, err = ReadLine(os.Stdin)
	}
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

// ReadLine reads a line from the reader with trailing \r dropped.
func ReadLine(reader io.Reader) ([]byte, error) {
	var line []byte
	var buffer [1]byte
	for {
		n, err := reader.Read(buffer[:])
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if n == 0 {
			continue
		}
		c := buffer[0]
		if c == '\n' {
			break
		}
		line = append(line, c)
	}
	return bytes.TrimSuffix(line, []byte{'\r'}), nil
}
