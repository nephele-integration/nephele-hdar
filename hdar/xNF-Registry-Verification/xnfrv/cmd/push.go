package cmd

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	v1 "github.com/opencontainers/image-spec/specs-go/v1"
	"github.com/spf13/cobra"
	oras "oras.land/oras-go/v2"
	"oras.land/oras-go/v2/content/file"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/registry"
)

var pushCmd = &cobra.Command{
	Use:   "push [ARTIFACT] [REGISTRY/REPOSITORY]",
	Args:  cobra.ExactArgs(2),
	Short: "Pushes a local artifact to a remote registry",
	Long: `Takes a local artifact and creates an OCI artifact which is then pushed to a remote registry.
			The artifact name and version is obtained from the local artifact tar.gz file.`,
	Run: PushCmdRun,
}

func ConfigurePushCmd(c *cobra.Command) {
	return
}

func PushCmdRun(c *cobra.Command, args []string) {
	// Check input info
	if len(args) != 2 {
		common.Logger.Info("Only accepts 2 argument [ARTIFACT] [REGISTRY/REPOSITORY] \n")
		return
	}
	fPath := args[0]
	rName := args[1]

	a, err := artifact.NewLocalArtifact(fPath)
	if err != nil {
		common.Logger.Info(err.Error())
		return
	} else if !InspectorConfig.AddCorrectInspector(a, true) {
		common.Logger.Info("Unable to add correct inspector implementation\ncheck naming convention and descriptor format&syntax")
		return
	}
	// At this point the version to file name convention has already been checked via AddCorrectInspector
	// Get name and version to complete the repo reference.
	di := a.Inspector.GetDescriptorInspector(a)
	nResult := di.GetUniqueName()
	vResult := di.GetVersion()

	// We need to ensure that no uniqueName:version was added to the registry/repo arg[1]
	if strings.HasSuffix(rName, fmt.Sprintf(fmt.Sprintf("%s:%s", nResult.Value, vResult.Value))) ||
		strings.HasSuffix(rName, fmt.Sprintf(fmt.Sprintf("%s/%s", rName, nResult.Value))) {
		common.Logger.Info("Please do not add artifact info to the registry. It will be obtained from the descriptor")
		return
	}
	repo, err := registry.NewRepoWithExistingDockerCredentials(fmt.Sprintf("%s/%s:%s", rName, nResult.Value, vResult.Value))
	if err != nil {
		common.Logger.Info("Invalid reference to the repository\n")
		common.Logger.Info(err.Error())
		return
	}
	// 0. Create a file store to build
	fs, err := file.New(".")
	if err != nil {
		common.Logger.Info("Unable to create local file store\n")
		common.Logger.Info(err.Error())
		return
	}
	defer fs.Close()
	ctx := context.Background()

	// fileDescriptors, err := artifact.GetLayersDescriptor(ctx, fs)
	// 1. Add files to a file store and create the manifest layer
	mediaType := artifact.MediaTypeLayer
	// Working with a single layer of the entire tar.gz
	// TODO: Does file-level layers reduce mem storage or improve layer sharing?
	filePaths := []string{filepath.Join(a.InMemoryArchive.FileRaw.Dir, a.InMemoryArchive.FileRaw.Name)}
	fileDescriptors := make([]v1.Descriptor, 0, len(filePaths))
	for _, path := range filePaths {
		fileDescriptor, err := fs.Add(ctx, a.InMemoryArchive.FileRaw.Name, mediaType, path)
		common.Logger.Infof("Descriptor Manifest Layer %+v\n", fileDescriptor)
		if err != nil {
			common.Logger.Info("Unable to create descriptor from layer\n")
			common.Logger.Info(err.Error())
			return
		}
		fileDescriptors = append(fileDescriptors, fileDescriptor)
	}
	// No additional annotations required for the layer

	// Prepare the config file
	f, _ := os.Create("config.json")
	defer f.Close()
	defer os.Remove("config.json")

	f.Write(a.ConfigContent)

	configDesc, err := fs.Add(ctx, "$config", a.ConfigMediaType, "config.json")
	if err != nil {
		common.Logger.Info("Unable to create config descriptor\n")
		common.Logger.Info(err.Error())
		return
	}
	configDesc.Annotations = make(map[string]string)

	// 2. Pack the files and tag the packed manifest
	annotations := make(map[string]string)
	// Add annotations at Manifest level so that we can see them in Harbor UI
	// (mixed with config content at the tag level UI)

	result := di.GetAnnotations()
	if result.Error != nil {
		common.Logger.Info("Error adding annotations\n")
		common.Logger.Info(result.Error.Error())
		return
	}
	annotations = result.Value

	packOpts := oras.PackOptions{
		PackImageManifest:   true,
		ConfigDescriptor:    &configDesc,
		ManifestAnnotations: annotations,
	}

	manifestDesc, err := oras.Pack(ctx, fs, "", fileDescriptors, packOpts)
	if err != nil {
		// TODO: Would be great to fix this somehow
		common.Logger.Info("Unable to Pack. Cannot push from where the artifact base folder is located\n")
		common.Logger.Info(err.Error())
		return
	}
	// oras.Pack generates the manifest as a json file and stores it in fs, which is PWD in our case.
	// we don't want it there. keep it in-memory only
	defer os.Remove(annotations["org.opencontainers.image.title"])
	common.Logger.Infof("manifest descriptor: %+v\n", manifestDesc)

	srcTag := di.GetVersion().Value
	dstTag := srcTag // we are not re-tagging
	if err = fs.Tag(ctx, manifestDesc, srcTag); err != nil {
		common.Logger.Info("Unable to tag manifest descriptor\n")
		common.Logger.Info(err.Error())
		return
	}

	// 3. Copy from the file store to the remote repository
	_, err = oras.Copy(ctx, fs, srcTag, repo, dstTag, oras.DefaultCopyOptions)
	common.Logger.Debugf("%s --- %s --- %+v", srcTag, dstTag, repo.Reference)
	if err != nil {
		common.Logger.Info("Unable to copy manifest to remote\n")
		common.Logger.Info(err.Error())
		return
	}
	common.Logger.Info("Pushed correctly\n")
	return
}
