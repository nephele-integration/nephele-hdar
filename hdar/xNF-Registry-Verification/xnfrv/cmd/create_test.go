package cmd

import (
	"testing"

	"github.com/stretchr/testify/mock"
)

type MockTemplate struct {
	mock.Mock
}

func (m *MockTemplate) GenerateLocalArtifact(baseDir string, templateContent string) bool {
	// Just make sure that the mock is reached
	m.On("GenerateLocalArtifact", baseDir, templateContent).Return(true)
	m.Called(baseDir, templateContent)
	return true
}

func TestCreateCmdRun(t *testing.T) {

	// Create an instance of the mock
	mockTemplate := new(MockTemplate)
	// Inject the mock into the function
	CMDGenerateLocalArtifact = mockTemplate.GenerateLocalArtifact

	// Create a dummy command
	cmd := createCmd

	// Test that with invalid Artifact Type it does not run
	args := []string{"INVALID"}
	cmd.SetArgs(args)
	cmd.Flags().Set("--destination", "flag0")

	// Call the function to be tested
	CreateCmdRun(cmd, args)
	mockTemplate.AssertNumberOfCalls(t, "GenerateLocalArtifact", 0)

	// Test with valid artifact Type
	args = []string{"VNF"}
	cmd.SetArgs(args)
	cmd.Flags().Set("--destination", "flag0")

	// Call the function to be tested
	CreateCmdRun(cmd, args)
	mockTemplate.AssertNumberOfCalls(t, "GenerateLocalArtifact", 1)
}
