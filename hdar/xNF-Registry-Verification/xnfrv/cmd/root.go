package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
	"golang.org/x/exp/maps"
)

var RootCmd = &cobra.Command{
	Use:   "xnfrv",
	Short: "Control tool for the xNF Registry and Verification",
	Long: `xNF Registry and Verification is a future-proof, full-fledged asset for Telco service storage, distribution and exposure
	Smart Networks & Services R&D unit in Eviden (ATOS business).
	Complete documentation is available at https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv`,
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// Default CMD configuration for the artifacts supported in this codebase
// InspectorConfig and InfoModelConfig shall be overwritten by projects
// extending the xNFRV (see NEPHELE's HDAR for an example)

type ConfigureDescriptorInspector struct {
	AddCorrectInspector func(a *artifact.Artifact, validateFileName bool) bool
}

type ConfigureTemplateMap struct {
	GetTemplateMap func() map[string]string
	GetSchemaMap   func() map[string]string
}

func (t *ConfigureTemplateMap) GetArtifactTypes() []string {
	// Used in help of create command. Does not need re-implementing in new projects
	aTypes := make([]string, 0)
	for v := range t.GetTemplateMap() {
		aTypes = append(aTypes, v)
	}
	return aTypes
}

var InspectorConfig = &ConfigureDescriptorInspector{
	AddCorrectInspector: artifact.AddCorrectInspector,
}

var InfoModelConfig = &ConfigureTemplateMap{
	GetTemplateMap: GetTemplateMap,
	GetSchemaMap:   GetSchemaMap,
}

func GetTemplateMap() map[string]string {
	// Same usage as for the descriptor and artifact inspector.
	// Configured at the cmd/root.go level
	templMap := artifact.OSMArtifactInspector{}.GetTemplateMap()
	maps.Copy(templMap, artifact.VReportArtifactInspector{}.GetTemplateMap())
	return templMap
}

func GetSchemaMap() map[string]string {
	// Same usage as for the descriptor and artifact inspector at the CLI level.
	// Configured at the cmd/root.go and overwritten in new implementations of xNFRV
	// This shall be include the maps for all implementations in this project
	schemaMap := artifact.OSMArtifactInspector{}.GetSchemaMap()
	maps.Copy(schemaMap, artifact.VReportArtifactInspector{}.GetSchemaMap())
	return schemaMap
}

func init() {
	// Hide internal command from Cobra
	RootCmd.CompletionOptions.DisableDefaultCmd = true
	ConfigurePullCmd(pullCmd)
	ConfigurePushCmd(pushCmd)
	ConfigureLoginCmd(loginCmd)
	ConfigureListCmd(listCmd)
	ConfigurePackageCmd(packageCmd)
	ConfigureCreateCmd(createCmd)
	ConfigureManifestCmd(manifestCmd)

	RootCmd.AddCommand(pullCmd)
	RootCmd.AddCommand(pushCmd)
	RootCmd.AddCommand(loginCmd)
	RootCmd.AddCommand(logoutCmd)
	RootCmd.AddCommand(listCmd)
	RootCmd.AddCommand(packageCmd)
	RootCmd.AddCommand(createCmd)
	RootCmd.AddCommand(lintCmd)
	RootCmd.AddCommand(manifestCmd)
}
