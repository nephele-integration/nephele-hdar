package cmd

import (
	"os"
	"path/filepath"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	"github.com/spf13/cobra"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/registry"
	"oras.land/oras-go/v2/content/file"
)

var pullCmd = &cobra.Command{
	Use:   "pull [REGISTRY/REPOSITORY/NAME:TAG]",
	Short: "Pulls an OCI artifact from a remote registry",
	Long:  `Downloads the files associated to an OCI artifact and optionally un-zips the content.`,
	Run:   PullCmdRun,
}

func ConfigurePullCmd(c *cobra.Command) {
	c.PersistentFlags().StringP("destination", "d", ".", "Base directory where the file will be downloaded")
	c.PersistentFlags().Bool("untar", false, "Indicates if downloaded artifact shall be kept as a GZ tarball (false) or extracted (true).")
	return
}

func PullCmdRun(c *cobra.Command, args []string) {
	// Check input info
	if len(args) != 1 {
		common.Logger.Info("Only accepts 1 argument [REGISTRY/REPOSITORY/NAME:TAG]\n")
		return
	}
	// 1. Prepare connection to remote registry
	// Check that args is correct (repo.Reference gives the info about reg, rep and image)
	repo, err := registry.NewRepoWithExistingDockerCredentials(args[0])
	if err != nil {
		common.Logger.Info("Unable to create repo\n")
		common.Logger.Info(err.Error())
		return
	}

	// 2. Create a file store
	// Prepare destination
	fBase, err := c.Flags().GetString("destination")
	if err != nil {
		common.Logger.Info("Unable to read destination flag\n")
		common.Logger.Info(err.Error())
		return
	}
	err = archive.CreateDirIfNotExist(fBase)
	if err != nil {
		common.Logger.Info("Unable to create destination dir\n")
		common.Logger.Info(err.Error())
		return
	}
	fs, err := file.New(fBase)
	if err != nil {
		common.Logger.Infof("Unable to create file store to pull artifact: %s\n", err.Error())
		return
	}
	defer fs.Close()
	specs, err := registry.Pull(repo, fs)
	if err != nil {
		common.Logger.Infof("Unable pull artifact: %s\n", err.Error())
		return
	}
	untar, err := c.Flags().GetBool("untar")
	if err != nil {
		common.Logger.Info("Unable to read untar flag\n")
		common.Logger.Info(err.Error())
		return
	} else if untar {
		common.Logger.Info("Proceeding to untar artifact content\n")
		// Single layer artifact support for now
		// TODO: we need a way to find the name of the inner directory name so that we can tell the client
		fName := specs.ImageManifest.Layers[0].Annotations["org.opencontainers.image.title"]
		common.Logger.Infof("File to untar %s\n", filepath.Join(fBase, fName))

		a, err := artifact.NewLocalArtifact(filepath.Join(fBase, fName))
		if err != nil {
			common.Logger.Info(err.Error())
			return
		} else if !InspectorConfig.AddCorrectInspector(a, false) {
			common.Logger.Info("Unable to add correct inspector implementation during pull\n")
			return
		}

		err = archive.UnGzip(a.InMemoryArchive, fBase)
		if err != nil {
			common.Logger.Infof("Error unzipping the artifact: %s\n", err.Error())
			return
		}
		common.Logger.Info("untarred artifact in destination directory\n")
		os.Remove(filepath.Join(fBase, fName)) // Only delete if correctly untar
	}
	return

}
