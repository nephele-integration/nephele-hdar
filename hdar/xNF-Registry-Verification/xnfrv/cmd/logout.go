package cmd

import (
	"context"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	credentials "github.com/oras-project/oras-credentials-go"
	"github.com/spf13/cobra"
)

var logoutCmd = &cobra.Command{
	Use:   "logout [REGISTRY]",
	Short: "Log out to a remote registry",
	Long:  `Log out to a remote registry. Access will be lost to all repositories.`,
	Args:  cobra.ExactArgs(1),
	Run:   LogoutCmdRun,
}

func LogoutCmdRun(cmd *cobra.Command, args []string) {
	ctx := context.Background()
	storeOpts := credentials.StoreOptions{}
	credStore, err := credentials.NewStoreFromDocker(storeOpts)
	if err != nil {
		common.Logger.Infof("Unable to load docker cred store %s\n", err.Error())
		return
	}
	if err = credentials.Logout(ctx, credStore, args[0]); err != nil {
		common.Logger.Info("Unable to correctly log out\n")
		return
	}
	common.Logger.Info("Logout Succeeded\n")
	return
}
