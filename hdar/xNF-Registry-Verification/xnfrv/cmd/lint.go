package cmd

import (
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
)

var lintCmd = &cobra.Command{
	Use:   "lint [PATH]",
	Short: "Verifies syntax",
	Long:  `Verifies syntax of a descriptor in folder or artifact and the structure`,
	Args:  cobra.ExactArgs(1),
	Run:   LintCmdRun,
}

func LintCmdRun(cmd *cobra.Command, args []string) {
	// Performs syntax and semantic verification of descriptor
	// Read the artifact
	arg_dir := strings.TrimSuffix(args[0], "/")
	fInfo, err := os.Stat(arg_dir)
	if err != nil {
		// error handling
	}
	var aName string
	if fInfo.IsDir() {
		// Create a Local Artifact
		tmpA, err := artifact.NewTmpArtifactFromFolder(arg_dir)
		if err != nil {
			common.Logger.Info(err.Error())
			return
		} else if !InspectorConfig.AddCorrectInspector(tmpA, false) {
			common.Logger.Errorf("Unable to add correct inspector implementation during tar\n")
			return
		}
		aName, err = artifact.TmpArtifactToLocalArtifact(tmpA, arg_dir)
	} else {
		aName = arg_dir
	}
	a, err := artifact.NewLocalArtifact(aName)
	if err != nil {
		common.Logger.Info(err.Error())
		// verification is automatically
	} else if !InspectorConfig.AddCorrectInspector(a, true) {
		common.Logger.Info("Verification failed")
	}
	return
}
