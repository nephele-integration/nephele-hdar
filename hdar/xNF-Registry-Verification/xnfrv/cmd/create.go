package cmd

import (
	"strings"

	"github.com/spf13/cobra"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/template"
)

var createCmd = &cobra.Command{
	Use:   "create [TYPE] [flags]",
	Short: "Create an artifact",
	Long:  `Create a baseline artifact for [TYPE=(VNF,NS)] based on template`,
	Args:  cobra.ExactArgs(1),
	Run:   CreateCmdRun,
}

// Used for mocks. We just want to test the Cobra logic
var CMDGenerateLocalArtifact = template.GenerateLocalArtifact

func ConfigureCreateCmd(c *cobra.Command) {
	c.PersistentFlags().StringP("destination", "d", ".", "Path to the base folder that will contain the artifact")
}

func CreateCmdRun(cmd *cobra.Command, args []string) {
	outputBasePath, _ := cmd.Flags().GetString("destination")
	aTypeFound := false
	for _, v := range InfoModelConfig.GetArtifactTypes() {
		if v == strings.ToUpper(args[0]) {
			aTypeFound = true
		}
	}
	if len(args) != 1 || !aTypeFound {
		common.Logger.Errorf("Expecting only the artifact type as argument. Choose %v\n", InfoModelConfig.GetArtifactTypes())
		return
	}
	tContent, ok := InfoModelConfig.GetTemplateMap()[strings.ToUpper(args[0])]
	if !ok {
		common.Logger.Error("Unable to obtain template content")
		return
	}
	common.Logger.Infof("Baseline path %s \n", outputBasePath)

	CMDGenerateLocalArtifact(outputBasePath, tContent)
	return
}
