package cmd

import (
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"

	"github.com/spf13/cobra"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/archive"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
)

var unTarCmd = &cobra.Command{
	Use:   "untar [PATH]",
	Short: "Extracts content from tar.gz file.",
	Long:  `Extracts the file or directory from a tar.gz file.`,
	Args:  cobra.ExactArgs(1),
	Run:   UnTarCmdRun,
}

func init() {
	packageCmd.AddCommand(unTarCmd)
}

func UnTarCmdRun(c *cobra.Command, args []string) {
	path, dPath, err := ValidatePackageCmdInput(args, c.Flags())
	if err != nil {
		common.Logger.Infof(err.Error())
		return
	}
	a, err := artifact.NewLocalArtifact(path)
	if err != nil {
		common.Logger.Infof(err.Error())
		return
	} else if !InspectorConfig.AddCorrectInspector(a, true) {
		common.Logger.Infof("Unable to add correct inspector implementation during untar\n")
		return
	}

	err = archive.UnGzip(a.InMemoryArchive, dPath)
	if err != nil {
		common.Logger.Infof("Unable to extract contents\n")
		common.Logger.Infof(err.Error())
		return
	}
	return
}
