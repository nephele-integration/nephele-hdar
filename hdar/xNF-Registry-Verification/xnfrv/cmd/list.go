package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"text/tabwriter"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/common"
	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/registry"
)

var listCmd = &cobra.Command{
	Use:   "ls [REGISTRY] [flags]",
	Short: "List all artifacts in a repository",
	Long:  `Shows all artifacts along with their mediaType`,
	Args:  cobra.ExactArgs(1),
	Run:   ListCmdRun,
}

func ConfigureListCmd(c *cobra.Command) {
	// Add option registry here??
	c.PersistentFlags().StringP("repository", "r", "", "Path to the repository. Empty for full root")
}

func ListCmdRun(cmd *cobra.Command, args []string) {
	repoPath, _ := cmd.Flags().GetString("repository")
	var contentMap map[string]string

	reg, err := registry.NewRegWithExistingDockerCredentials(args[0])
	if err != nil {
		common.Logger.Infof("Unable to create registry %s\n", err.Error())
		return
	}
	orderedRepos, err := registry.List(reg)
	w := tabwriter.NewWriter(os.Stdout, 1, 1, 1, ' ', tabwriter.Debug|tabwriter.DiscardEmptyColumns)
	fmt.Fprintln(w, "Artifact\tmediaType\t")
	for _, k := range orderedRepos {
		if k == repoPath {
			common.Logger.Debugf("Ignoring %s\n", k)
		}
		fmt.Fprintf(w, "%s\t%s\t\n", k, contentMap[k])
	}

	w.Flush()
	return
}
