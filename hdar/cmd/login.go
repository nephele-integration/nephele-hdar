package cmd

import (
	"github.com/spf13/cobra"
	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var loginCmd = &cobra.Command{
	Use:   "login [REGISTRY] [flags]",
	Short: "Logs in to the HDAR",
	Long:  `Logs in to HDAR. Registry might be a url or ip:port. No repository is needed.`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfrcmd.LoginCmdRun,
}
