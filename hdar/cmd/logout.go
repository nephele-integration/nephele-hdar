package cmd

import (
	"github.com/spf13/cobra"
	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var logoutCmd = &cobra.Command{
	Use:   "logout [REGISTRY]",
	Short: "Logs out from the HDAR",
	Long:  `Logs out from the HDAR. Access will be lost to all repositories.`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfrcmd.LogoutCmdRun,
}
