package cmd

import (
	"fmt"
	"os"

	"github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/hdar/pkg/artifact"
	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
	xnfrartifact "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/pkg/artifact"
	"golang.org/x/exp/maps"

	"github.com/spf13/cobra"
)

var RootCmd = &cobra.Command{
	Use:   "hdarctl",
	Short: "Control tool for the HDA Registry",
	Long:  `Hyper Distributed Application Registry Control toolkit`,
}

func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	// Hide internal command from Cobra
	RootCmd.CompletionOptions.DisableDefaultCmd = true
	// Extend the xNF CMD with custom implementations
	xnfrcmd.InspectorConfig.AddCorrectInspector = artifact.AddCorrectInspector
	xnfrcmd.InfoModelConfig.GetTemplateMap = GetTemplateMap
	xnfrcmd.InfoModelConfig.GetSchemaMap = GetSchemaMap
	xnfrcmd.ConfigurePushCmd(pushCmd)
	xnfrcmd.ConfigurePullCmd(pullCmd)
	xnfrcmd.ConfigureLoginCmd(loginCmd)
	xnfrcmd.ConfigurePackageCmd(packageCmd)
	xnfrcmd.ConfigureCreateCmd(createCmd)
	xnfrcmd.ConfigureManifestCmd(manifestCmd)

	RootCmd.AddCommand(pullCmd)
	RootCmd.AddCommand(pushCmd)
	RootCmd.AddCommand(loginCmd)
	RootCmd.AddCommand(logoutCmd)
	RootCmd.AddCommand(packageCmd)
	RootCmd.AddCommand(createCmd)
	RootCmd.AddCommand(lintCmd)
	RootCmd.AddCommand(manifestCmd)

}

func GetTemplateMap() map[string]string {
	// Extends the template map from xNF
	templMap := xnfrartifact.OSMArtifactInspector{}.GetTemplateMap()
	maps.Copy(templMap, artifact.HdaGraphArtifactInspector{}.GetTemplateMap())
	maps.Copy(templMap, artifact.WoTArtifactInspector{}.GetTemplateMap())
	return templMap
}

func GetSchemaMap() map[string]string {
	// Extends the schema map from xNF
	schemaMap := xnfrartifact.OSMArtifactInspector{}.GetSchemaMap()
	maps.Copy(schemaMap, artifact.HdaGraphArtifactInspector{}.GetSchemaMap())
	maps.Copy(schemaMap, artifact.WoTArtifactInspector{}.GetSchemaMap())
	return schemaMap
}
