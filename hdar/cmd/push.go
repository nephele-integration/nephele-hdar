package cmd

import (
	"github.com/spf13/cobra"
	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var pushCmd = &cobra.Command{
	Use:   "push [ARTIFACT] [REGISTRY/REPOSITORY]",
	Short: "Pushes a local artifact to the HDAR",
	Long:  `Takes a local artifact and creates an OCI image which is then pushed to a HDAR`,
	Run:   xnfrcmd.PushCmdRun,
}
