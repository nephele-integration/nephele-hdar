package cmd

import (
	"github.com/spf13/cobra"
	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var pullCmd = &cobra.Command{
	Use:   "pull [REGISTRY/REPOSITORY/NAME:TAG]",
	Short: "Pulls an OCI artifact from a the HDAR",
	Long:  `Downloads the files associated to an OCI artifact and optionally extracts the content.`,
	Run:   xnfrcmd.PullCmdRun,
}
