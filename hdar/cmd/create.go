package cmd

import (
	"github.com/spf13/cobra"

	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var createCmd = &cobra.Command{
	Use:   "create [TYPE] [flags]",
	Short: "Create an artifact [TYPE=(VNF,NS,HDAG,WOT)]",
	Long:  `Create a baseline artifact for [TYPE=(VNF,NS,HDAG,WOT)] based on template`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfrcmd.CreateCmdRun,
}
