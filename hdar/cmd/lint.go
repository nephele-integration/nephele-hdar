package cmd

import (
	"github.com/spf13/cobra"

	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var lintCmd = &cobra.Command{
	Use:   "lint [PATH]",
	Short: "lint an artifact",
	Long:  `Lint verification of an artifact based on the folder`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfrcmd.LintCmdRun,
}
