package cmd

import (
	"github.com/spf13/cobra"
	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var manifestCmd = &cobra.Command{
	Use:   "manifest [REGISTRY/REPOSITORY/NAME:TAG] [FLAGS]",
	Short: "Pulls an OCI manifest. ",
	Long:  `Downloads the the OCI manifests for image and config.`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfrcmd.ManifestCmdRun,
}
