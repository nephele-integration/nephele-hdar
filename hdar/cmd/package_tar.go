package cmd

import (
	"github.com/spf13/cobra"

	xnfr_cmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var tarCmd = &cobra.Command{
	Use:   "tar [PATH]",
	Short: "Package PATH as a tar.gz file.",
	Long:  `Packages the directory pointed by PATH as a tar.gz file.`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfr_cmd.TarCmdRun,
}

func init() {
	packageCmd.AddCommand(tarCmd)
}
