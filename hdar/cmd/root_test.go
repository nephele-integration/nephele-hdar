package cmd

import (
	"sort"
	"testing"
)

func TestFindCommands(t *testing.T) {
	// Check if all commands exist
	foundCommands := make([]string, 0)                                                                       // adding the len creates empty lines in cmd output....
	expectedCommands := []string{"push", "pull", "login", "logout", "package", "create", "lint", "manifest"} // tar and untar are subcmmands
	for _, command := range RootCmd.Commands() {
		foundCommands = append(foundCommands, command.Name())
	}
	CompareArraysAndRaiseError(foundCommands, expectedCommands, t)
	// Check Package command
	foundCommands = make([]string, 0) // adding the len creates empty lines in cmd output....
	expectedCommands = []string{"tar", "untar"}
	for _, command := range packageCmd.Commands() {
		foundCommands = append(foundCommands, command.Name())
	}
	CompareArraysAndRaiseError(foundCommands, expectedCommands, t)
}

func CompareArraysAndRaiseError(foundCommands, expectedCommands []string, t *testing.T) {
	sort.Strings(foundCommands)
	sort.Strings(expectedCommands)

	if len(foundCommands) != len(expectedCommands) {
		t.Errorf("Expected commands not found:\n Expected%+v\nFound:%+v", expectedCommands, foundCommands)
	}

	// Compare each element
	for i := 0; i < len(expectedCommands); i++ {
		if expectedCommands[i] != foundCommands[i] {
			t.Errorf("Expected commands %s not found, instead %s", expectedCommands[i], foundCommands[i])
		}
	}
}
