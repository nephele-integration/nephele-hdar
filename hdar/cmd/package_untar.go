package cmd

import (
	"github.com/spf13/cobra"

	xnfr_cmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var unTarCmd = &cobra.Command{
	Use:   "untar [PATH]",
	Short: "Extracts content from tar.gz file.",
	Long:  `Extracts the file or directory from a tar.gz file.`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfr_cmd.UnTarCmdRun,
}

func init() {
	packageCmd.AddCommand(unTarCmd)
}
