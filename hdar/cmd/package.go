package cmd

import (
	"github.com/spf13/cobra"
	xnfrcmd "github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-Registry-Verification/xnfrv/cmd"
)

var packageCmd = &cobra.Command{
	Use:   "package tar/untar [flags]",
	Short: "Parent command to tar and untar commands",
	Long:  `Parent command to work with local artifacts`,
	Args:  cobra.ExactArgs(1),
	Run:   xnfrcmd.PackageCmdRun,
}
