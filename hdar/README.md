**Main**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdar-coverage-main.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdar-version-main.svg)

**Develop**
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdar-coverage-develop.svg)
![Not found](https://github.gsissc.myatos.net/GLB-BDS-ETSN-SECURITY/xNF-RV-NEPHELE-HDAR/wiki/hdar-version-develop.svg)

[[_TOC_]]

# HDA Registry

**The HDA-Registry (HDAR) is the common storage, distribution and verification system for all the artifacts involved in the deployment of HDA via the Nephele SMO framework.** Each artifact is stored in a repository inside the HDAR where all tags of the same artifact are available.

HDAR leverages an Open Container Initiative ([OCI](https://opencontainers.org/)) compatible image Registry as Storage ([ORAS](https://oras.land/)) to finally align the storage, distribution and exposure mechanisms for artifacts of the Vertical Service Providers (VSP) and the Communication Service Providers (CSP).

![Summary](./docs/summaryHDAR.png)

Note: The OCI config manifest be requested to the HDAR REST API to known what the artifact contains and how to process it before accessing the actual artifact.

## Description

The HDAR is built as the combination of multiple components

![Architecture](./docs/HDAR_architecture.png)

- The Control CLI currently focuses on: Interacts with the registry via **nephele-platform.netmode.ece.ntua.gr**
1. CLI to interact with the HDAR will be provided as a single [executable file for ubuntu envs](./hdarctl).
2. Basic interaction with an OCI-Registry for a custom type of artifact allowing to push OSM NS and VNF and Nephele applications
3. Development toolkit to manage the artifacts involved in a HDAG. 
  - Templating mechanism to create Nephele applications
  - Verification of syntax, semantic and integrity of components
4. **The HDAR is prepared to be interacted with by devs using CLI tools**:
  - docker
  - helm (v3): No support for V2 repo index.yaml
  - hdar: commands following the Helm structure. Library and CLI to interact with the Hyper Distributed Application Registry.

- The REST API currently focuses on: Available **nephele-platform.netmode.ece.ntua.gr/hdarapi/swagger/index.html**
1. Custom functionalities for ease the use of the Nephele meta-orchestration framework
2. Service catalogue and discovery

- The Verification Engine focuses on: Available indirectly via the HDAR REST API
1. Verification of syntax, semantic and integrity of components
2. Scanning of Docker images and VM images searching for CVE
3. Scanning Helm charts and OSM descriptors searching for missconfigurations


## Getting Started

Check deliverable D2.1, D2.2 and D4.1

### Prerequisites

Full configuration via ENV Vars:

- Check $PWD/cicd/.env-test for local deployment and move it to $PWD/.env
- Equivalent variables are available in the helm chart's values.yaml adn configmaps

#### Software

Project built using Golang 1.19 and requires to have an OCI Registry installed and accessible -> **nephele-platform.netmode.ece.ntua.gr**.

#### Hardware

No major requirements found. 1vCPU, 1GiB RAM, it needs a persistance volume of at least 50 GiB 

### Installation

Project developed using WSL Golang installation

#### Locally

    ```bash
    go mod tidy
    # Make sure API docs are updated
    cd api
    go get github.com/swaggo/swag/cmd/swag@latest
    go get github.com/swaggo/http-swagger
    go install github.com/swaggo/swag/cmd/swag@latest
    go install github.com/swaggo/http-swagger
    PATH=$(go env GOPATH)/bin:$PATH
    swag init
    cd ..
    go fmt $(go list ./... | grep -v /vendor/)  #<-- Execute before merging to develop
    go vet $(go list ./... | grep -v /vendor/)  #<-- Automated with cicd pipeline
    ```

    Build and use

    ```bash
    GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o hdarctl . \
    GOOS=linux GARCH=amd64 CGO_ENABLED=0 go build -v -a -installsuffix cgo -o hdarapi ./api
    ./hdarctl --help
    ./hdarapi
    ```

    Build image
    ```bash
    docker build registry.atosresearch.eu:18498/hdar:latest --network host -f cicd/dockerfile_tests . 
    ```
#### K8s

0. Configure the K8s cluster. 
- Either request the kubeconfig file to guillermo.gomezchavez@eviden.com or create your own, build your own images and push to a registry
- Adapt the values.yaml to update the images and secrets to pull them
1. Install the OCI Registry if not available 
```bash
helm install hdar-distribution cicd/distribution-chart/
```
2. Install the HDAR Manager
```bash
helm install hdar-manager cicd/hdar-manager/
```

## Usage

This project contains the source code to deploy 2 different micro-services (Additional microservices are integrated directly in the hdar-install procedure)

1. Local CLI for developers --> ./hdarctl --help 
2. K8s REST API --> ./hdarapi

The folder hdar-examples contains the relevant information about the artifacts and their IM

Check hdar-install folder for K8s helm chart

## Documentation

Check D2.1, D2.2 and D4.1

## API documentation

CLI commands :

```bash
./hdarctl --help
Hyper Distributed Application Registry Control toolkit

Usage:
  hdarctl [command]

Available Commands:
  create      Create an artifact [TYPE=(VNF,NS,HDAG,WOT)]
  help        Help about any command
  lint        lint an artifact
  login       Logs in to the HDAR
  logout      Logs out from the HDAR
  manifest    Pulls an OCI manifest. 
  package     Parent command to tar and untar commands
  pull        Pulls an OCI artifact from a the HDAR
  push        Pushes a local artifact to the HDAR

Flags:
  -h, --help   help for hdarctl

Use "hdarctl [command] --help" for more information about a command.
```

REST API available as a ![Swagger YAML](./api/docs/swagger.yaml). Online here **nephele-platform.netmode.ece.ntua.gr/hdarapi/swagger/index.html**

## Testing

Golang tests are provided as part of the source code.

```bash
    go test $(go list ./... | grep -v /vendor/)  #<-- Automated with cicd pipeline
```

## Roadmap

- Sep 2023 -> PoC 
- Oct 2023 -> First demo in OSM Ecosystem day
- Feb 2024 -> First integrated version in NTUA environment
- Jun 2024 -> Functional version ready for UCs
- Ago 2024 -> Improvements 
- Ago 2025 -> Final release of the project

## Contribution

Tech:
- Guillermo Gomez guillermo.gomezchavez@eviden.com

Asset Owner:
- Sonia Castro sonia.castro@eviden.com

A Gitflow methodology is implemented within this repo. Proceed as follows:

1. Open an Issue, label appropriately and assign to the next planned release.
2. Pull the latest develop and branch off of it with a branch or MR created from GiLab.
3. Commit and push frequently.
4. When ready, open a MR to develop and wait for approval.

## License

ATOS and Eviden Copyright applies. Apache License

```text
/*
 * Copyright 20XX-20XX, Atos Spain S.A.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
```
