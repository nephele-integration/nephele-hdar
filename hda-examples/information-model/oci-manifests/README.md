Each file contains the two manifest required by OCI with the following format:

```json
{
    "image":
        {   
            "mediaType": "",
            "content": {}
        },
    "config":
        {   
            "mediaType": "",
            "content": {}
        },
}
```