**Repository hosting the Hyper Distributed Application Information Models for developers**
[__TOC__]

# Background technologies

The HDAR is not a DB for hosting plain files. It is an implementation of the OCI-Distribution specification to harmonize how artifacts are stored in the compute continuum UCs.

What is the OCI specification and how it applies to NEPHELE?

![Background info](./docs/background_info.png)

All artifacts are based on a local folder that contains, in the simplest scenario, a single descriptor file. CLI tools are used to package them into a tar.gz file and push/pull them. **As per the OCI specifications an additional manifest is created automatically to assist the management of the artifact and most importantly the work of the system of person that needs to execute the artifact.**

What are the artifacts in an HDAG?

![Artifacts](./docs/artifacts.png)

# NEPHELE HDA Information Models

The Nephele SMO framework gets a HDA Graph and deploys service (K8s workloads and NSs) described inside of it.

Summary of artifacts related to the Nephele project:

- **Docker images**: IM not described here
  - VO base images
  - apps images
  - (if needed) init-containers to configure VOs
- **App Helm charts**: IM not described here
  - Must include in its logic interactions with VOs.
  - The HDA Graph will provide the grounded information to connect to them and can be injected as valuesOverwrite.
- **CRD Operator Helm Chart**
  - needed to be available in all clusters that might host a CRD (c)VO
- **(c)VO Helm charts for all implementations (WoT, OMA, CRD).**
  - configuration files are written in jinja format to be rendered by helm and passed to the container via volume at instantiation time.
  - templates are filled with default values.yaml and overwritten at instantiation-time via the HDA Graph as valuesOverwrite
  - any dependent chart can be added following Helm documentation (in charts/ directory) or deploy it first in the HDAG as an app Helm Chart.
- **HDA Graph**
  - Descriptor with the selection of artifacts ((c)VO, apps etc ) and the injection of grounded and ungrounded information via *valuesOverwrite* key to reach App and (c)VO Helm Charts
  - Default intent-based orchestration descriptor. Can be replaced with new intent at instantiation time.
- **OSM NS and KNFs**: IM not described here
- (if needed) ormb-based AI/ML models

**Some notes**:
- Everything that is instantiated by the SMO framework will consist on a Helm Chart.
  - The 3 implementations of VOs are stored as Helm charts
- **The HDAG descriptor is not a helm chart since it is used to orchestrate the instantiation of apps and VOs but is not directly instantiated.**
- VO implementors will have the capability to inject new values at HDAG design time.
- There are 2 types of artifacts, grounded (they have been instantiated by the SMO framework) and ungrounded (corresponds to the packages containing the descriptors, charts and images, they are available in the HDAR)
- More documentation after D4.1

Since we are creating OCI image-spec for all these artifacts we need to comply with:

-	Artifact package name convention name-version.tar.gz or .tgz
-	Version or tag following SemVer3
-	ID contain at most 63 characters
-	ID contain only lowercase alphanumeric characters or '-'. '_' is allowed in some cases but not recommended
-	ID start and end with an alphanumeric character
-	Image manifest JSON as defined in application/vnd.oci.image.manifest.v1+json mediaType in order to be compatible with the distribution-spec
-	Following the strategy used by Helm, HDAG will also be released as a GZ-compressed tar artifact which corresponds to the expected filesystem bundle.
-	The structure of files in an artifact and IM of the descriptor must provide enough information to find out the application mediaType.
-	All mediaType names must follow the iana convention


## HDA Graph

Deployment unit for the Nephele SMO framework,

Artifact structure fixed:

```
name/
  descriptor.yaml
  intent/
    default_intent.yaml
```

### OCI manifests

Still a very early approach (extenden in [NFV artifacts](hda-examples/information-model/oci-manifests/nfv-v1-specification-manifest-example.json))

| Version      | Release    | Schema file |
|--------------|------------|-------------|
| v1.0.0       | 31/10/2023 | hda-examples/information-model/oci-manifests/hdag-v1-specification-manifest-example.json     |

### descriptor.yaml

Connects Helm Charts and NSs, provides ungrounded information (injected via values.yaml) and provides the needed information to configure the Nephele SMO framework (monitoring, LCM ... )

| Version      | Release    | Schema file |
|--------------|------------|-------------|
| v0.1.0       | 31/10/2023 | hda-examples/information-model/hdag/hda_graph-0.1.0.yaml     |

### intent.yaml

Connects Helm Charts and NSs, provides ungrounded information (injected via values.yaml) and provides the needed information to configure the Nephele SMO framework (monitoring, LCM ... )

| Version      | Release    | Schema file |
|--------------|------------|-------------|
| v0.1.0       | 31/10/2023 | hda-examples/information-model/hdag/intent/intent-0.1.0.yaml     |


## (c)VO Helm charts for WoT

Not deployed directly, always referenced from a HDAG. From the HDAR perspective, VO and cVO do not need different approaches, the difference is only in the app logicand configuration.

Artifact structure fixed for all versions of the VO WOT:

```
name/
  values.yaml
  Chart.yaml
  charts/
  scripts/
    app.py
    config.yaml
    td.json
  templates/
    configmap.yaml
    deployment.yaml
```

### OCI manifests

It is a helm chart. We should not change it.

| Version      | Release    | Schema file |
|--------------|------------|-------------|
| v1.0.0       | 31/10/2023 | hda-examples/information-model/oci-manifests/VO-helm-specification-manifest-example.json     |


### app.py

ToDo: Document how to comply with the WoTpy image container

### config.yaml

scripts/config.yaml is the descriptor that defines the VO WOT implementation
(IM developed by NTUA)


| Version      | Release    | Schema file |
|--------------|------------|-------------|
| v0.1.0       | 31/10/2023 | hda-examples/information-model/vo_wot/config-0.1.0.yaml     |


### td.json

ToDo: Document where is the specification

# Workflow for HDA developers

Example of the creation of an E2E UCs which deploys a VO and some applications consuming the VO.
The folder "packages" includes a WIP example aligned with this workflow. In case of replicating focus on the package and push parts of the workflow since the examples should be ready to be used.

## Example for WOT implementation (hdarctl v0.1.0)

![dev workflow](./docs/dev_workflow.png)

# Demo with HDAR in NTUA

1. Request USER, PASSWORD to guillermo.gomezchavez@eviden.com
2. Execute
```bash
./hdarctl  login nephele-platform.netmode.ece.ntua.gr -u YOUR_USER -p YOUR_PASSWORD
./hdarctl lint wot-plenary/vo1
helm package wot-plenary/vo1 -d .
helm push vo1-0.1.0.tgz oci://nephele-platform.netmode.ece.ntua.gr
./hdarctl lint wot-plenary/hda_graph_wot_1 
./hdarctl package tar wot-plenary/hda_graph_wot_1 -d .
./hdarctl push hda_graph_wot_1-0.1.0.tar.gz nephele-platform.netmode.ece.ntua.gr
```
