# xNF-RV-NEPHELE-HDAR

This multi-project repository hosts the commitments of T4.1. Please refer to:
1. WP2 and WP4 for further documentation.
2. [Code + install README](./hdar/README.md)
3. [HDA and developers README](./hda-examples/README.md)

- hda-examples: Development environment documentation containing demo packages and IM for the Nephele's HDAs.
    + PoC tested with GitHub Container Registry as OCI Registry. [Compatibility list](https://oras.land/docs/compatible_oci_registries)
    + First demo tested with Harbor as OCI Registry
    E2E HDA based on the WOT implementation of VOs during the WOT Plenary
    Information Model version control for all nephele-defined descriptors.
- hdar: 
    - CMD: Control CLI (hdarctl) built on top of the xNFRV SDK
    - API: HDAR Manager REST system based on OpenAPI.
    - cicd: Installation procedure for the Nephele project
        + Final selection is the baseline Distribution as OCI Registry

![Full T4.1](./hdar/docs/t4-1.png)